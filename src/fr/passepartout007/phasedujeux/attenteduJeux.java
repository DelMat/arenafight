package fr.passepartout007.phasedujeux;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import fr.passepartout007.gestionbasedonnee.ListdesJoueur;
import fr.passepartout007.gestionjoueur.Joueur;
import fr.passepartout007.gestionjoueur.Mode;
import fr.passepartout007.gestionterrain.SaveNewArena;
import fr.passepartout007.lancementplugin.lancement;
import fr.passepartout007.modeDeJeux.Cauchemar;
import fr.passepartout007.modeDeJeux.Duel;
import fr.passepartout007.modeDeJeux.TonteBouftou;
import fr.passepartout007.scoreboard.GestionScoreboard;

public class attenteduJeux implements Listener {

	private Location loctionSpawn;
	public transient lancement lancement;
	public attenteduJeux(lancement lancement) {
	this.lancement = lancement;
		
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e){
		Player player = e.getPlayer();
		Action action = e.getAction();
		if( e.getClickedBlock()==null) {
			return;
		}
		Location location = e.getClickedBlock().getLocation();


		if ( location == null) {
			return;
		}

		Location locationpanneau = location.add(0, 1, 0);
		World world = player.getWorld();
		Duel duel = new Duel(lancement);
		Cauchemar cauchemar = new Cauchemar(lancement);
		ListdesJoueur listJ = new ListdesJoueur();
		
		if(world.getBlockAt(locationpanneau) != null &&  action == Action.RIGHT_CLICK_BLOCK )
		{
			BlockState bs = world.getBlockAt(locationpanneau).getState();
			
			if (bs instanceof Sign){
				Sign sign = (Sign) bs;
	
				if(sign.getLine(0).equalsIgnoreCase("�e�lMode de jeux")) {
					
					
				
					String NomMDJ = duel.getnextmodeDeJeux(player);
					
					
						sign.setLine(2, NomMDJ);
						sign.update();
						GestionScoreboard gs = new GestionScoreboard();
						gs.MajScoreboardJeux(lancement);
				}
				
				if(sign.getLine(1).equalsIgnoreCase("�e�lJoueur")) {
					Joueur joueur = new Joueur(player.getName(),Mode.JOUEUR);
					listJ.suppJoueurtolist(joueur);
					listJ.addJoueurtolist(joueur);
					player.sendMessage("�btu passe joueur");
					GestionScoreboard gs = new GestionScoreboard();
					gs.MajScoreboardJeux(lancement);
				}
				
				if(sign.getLine(1).equalsIgnoreCase("�e�lSpectateur")) {
					Joueur joueur = new Joueur(player.getName(),Mode.SPECTATEUR);
					listJ.suppJoueurtolist(joueur);
					listJ.addJoueurtolist(joueur);
					player.sendMessage("�btu passe Specateur");
					GestionScoreboard gs = new GestionScoreboard();
					gs.MajScoreboardJeux(lancement);
				}
				
				
				if(sign.getLine(0).equalsIgnoreCase("Save Arena")) {
					SaveNewArena newArena = new SaveNewArena();
					newArena.exportNewArena(player, (sign.getLine(1)));									
				}
				
				if(sign.getLine(0).equalsIgnoreCase("�e�lArena")) {
				
					SaveNewArena newArena = new SaveNewArena();
					 ArrayList<String> listArene = newArena.ArenaListName();
					 String namePanneau = sign.getLine(2);
			
					 for(String nameArene : listArene) {
						
						 if(("�c�l" +nameArene).equalsIgnoreCase(namePanneau)) {
							 
							 if( listArene.size() -1 == listArene.indexOf(nameArene)) {
								 sign.setLine(2, "�c�l" + listArene.get(0));
									sign.update();
				
					
									return;
							 }else {
								 sign.setLine(2,"�c�l" + listArene.get( listArene.indexOf(nameArene)+1));
								sign.update();
		
								return;
							 }
						 }

						 if(namePanneau.equalsIgnoreCase(sign.getLine(2))) {
							 sign.setLine(2,"�c�l" + listArene.get(0));
							 sign.update();
								GestionScoreboard gs = new GestionScoreboard();
								gs.MajScoreboardJeux(lancement);
						 }
					 }	
				
				}
				if(sign.getLine(0).equalsIgnoreCase("test")) {
					
					HashMap<String,Integer> listtrier = new  HashMap<String,Integer>();
					listtrier.put("Passepartout007", 61);
					listtrier.put("Blazy", 95);
					listtrier.put("max", 21);
					listtrier.put("patate", 68);
					listtrier.put("patate545", 61);
					
					System.out.println(listtrier);
					TonteBouftou lst = new TonteBouftou(lancement); 
					
		
				
				}
				
				if(sign.getLine(1).equalsIgnoreCase("�e�lStart")) {
					LancementDuJeux ldj = new LancementDuJeux(player,lancement);
					ldj.Start(player);
					
				}
			}	
		}
	}

	public Location getLoctionSpawn(World world) {
		this.setLoctionSpawn(world);
		return loctionSpawn;
	}

	public void setLoctionSpawn(World world) {
		this.loctionSpawn = new Location(world,  -1295,48, -1166);
	}

}
