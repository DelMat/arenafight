package fr.passepartout007.phasedujeux;

import java.io.Serializable;

import fr.passepartout007.gestionbasedonnee.ListdesJoueur;
import fr.passepartout007.gestionterrain.ArenaParam;
import fr.passepartout007.modeDeJeux.modeDejeux;

public class ParametreDePartie implements Serializable {
	private boolean encours;
	private ArenaParam arene;
	private modeDejeux ModeDeJeux;
	private ListdesJoueur ListJoueur;

	
	
	public modeDejeux getModeDeJeux() {
		return ModeDeJeux;
	}

	public void setModeDeJeux(modeDejeux modeDeJeux) {
		this.ModeDeJeux = modeDeJeux;
	}

	public ArenaParam getArene() {
		return arene;
	}

	public void setArene(ArenaParam arene) {
		this.arene = arene;
	}

	public ListdesJoueur getListJoueur() {
		return ListJoueur;
	}

	public void setListJoueur(ListdesJoueur listJoueur) {
		ListJoueur = listJoueur;
	}

	public boolean isEncours() {
		return encours;
	}

	public void setEncours(boolean encours) {

		this.encours = encours;
	}
}
