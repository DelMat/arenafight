package fr.passepartout007.phasedujeux;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import fr.passepartout007.gestionbasedonnee.ListdesJoueur;
import fr.passepartout007.gestionbasedonnee.basededonne;
import fr.passepartout007.gestionjoueur.EquipementJoueur2;
import fr.passepartout007.gestionjoueur.Joueur;
import fr.passepartout007.gestionjoueur.Mode;
import fr.passepartout007.gestionterrain.Arena;
import fr.passepartout007.lancementplugin.lancement;
import fr.passepartout007.modeDeJeux.Cauchemar;
import fr.passepartout007.modeDeJeux.DesACoudre;
import fr.passepartout007.modeDeJeux.Duel;
import fr.passepartout007.modeDeJeux.FloconFall;
import fr.passepartout007.modeDeJeux.NoMode;
import fr.passepartout007.modeDeJeux.TonteBouftou;
import fr.passepartout007.modeDeJeux.modeDejeux;
import fr.passepartout007.modeDeJeux.speedJump;

public class LancementDuJeux {
	public Location locModeJeuxArena;
	public Location locNomArena; 
public transient lancement lancement;
	public LancementDuJeux(Player player,lancement lancement){
		this.lancement =lancement;
		
		this.locModeJeuxArena = new Location(player.getWorld(),-1290,49,-1176);
		this.locNomArena = new Location(player.getWorld(),-1290,49,-1172);
		
	}
	public void Start(Player player) {
		World world = player.getWorld();
		Sign signModeJeuxArena = null;
		Sign signNomArena = null;
		if(world.getBlockAt(locModeJeuxArena) != null && world.getBlockAt(locNomArena) != null ){
			BlockState bslMJA = world.getBlockAt(locModeJeuxArena).getState();
			BlockState bsNA = world.getBlockAt(locNomArena).getState();
			
			if (bslMJA instanceof Sign && bsNA instanceof Sign){
				signNomArena = (Sign) bslMJA;
				signModeJeuxArena = (Sign) bsNA;
			}
		}

		modeDejeux mdj = new NoMode(lancement);
		Arena arena = new Arena();
		EquipementJoueur2 equipementJoueur = new EquipementJoueur2();
		equipementJoueur.exportEquipement();
		ListdesJoueur listJoueur = new ListdesJoueur();
		basededonne bdd = new basededonne();

		
		
		
		
		
		ParametreDePartie paramparti = new ParametreDePartie();
		paramparti.setModeDeJeux(mdj.getModedeJeux(signModeJeuxArena.getLine(2)));
		paramparti.setArene(arena.importArenaParameter(signNomArena.getLine(2).substring(4)));
	
		paramparti.setListJoueur(listJoueur);

		//V�rification sur les parametres de partie 
	
		String nomMdj = paramparti.getModeDeJeux().getName();
		if( !(paramparti.getArene().modeDeJeuxExist(nomMdj))){
			player.sendMessage("D�sol� le mode de jeux n'existe pas pour cette ar�ne");
			return;
		}
	

		//export des paramettre de partie
		bdd.exportParametrePartie(paramparti);
		

		// si le mode de jeux est Duel alors
		if(paramparti.getModeDeJeux().getName().equalsIgnoreCase("�c�lDuel")) {
			int nbjoueur = 0;
			for(Joueur joueur: paramparti.getListJoueur().getlistJoueur()) {
				
				if(joueur.getMode() == Mode.JOUEUR) {
					nbjoueur = nbjoueur +1;
				}
			}
			
			if((nbjoueur!=2)) {
				player.sendMessage("D�sol� le nombre de joueur pour le Duel doits �tre �gale � 2");
				return;
			}

			Duel duel = new Duel(lancement);
			duel.lancementDePartie();
		}
		// si le mode de jeux est FloconFall alors
		if(paramparti.getModeDeJeux().getName().equalsIgnoreCase("�c�lFloconFall")) {
			int nbjoueur = 0;
			for(Joueur joueur: paramparti.getListJoueur().getlistJoueur()) {
				
				if(joueur.getMode() == Mode.JOUEUR) {
					nbjoueur = nbjoueur +1;
				}
			}
			
			if((nbjoueur<2)) {
				player.sendMessage("D�sol� le nombre de joueur pour le FloconFall doits �tre au moins de 2");
				return;
			}

			FloconFall floconFall = new FloconFall(lancement);
			floconFall.lancementDePartie();
		}
		
		// si le mode de jeux est DesACoudre alors
		if(paramparti.getModeDeJeux().getName().equalsIgnoreCase("�c�lDesACoudre")) {
			int nbjoueur = 0;
			for(Joueur joueur: paramparti.getListJoueur().getlistJoueur()) {
				
				if(joueur.getMode() == Mode.JOUEUR) {
					nbjoueur = nbjoueur +1;
				}
			}
			
			if((nbjoueur<1)) {
				player.sendMessage("D�sol� le nombre de joueur pour le DesACoudre doits �tre au moins de 1");
				return;
			}

			DesACoudre desACoudre = new DesACoudre(lancement);
			desACoudre.lancementDePartie();
		}
		
		//Si le mode de jeux est Cauchemar est alors
		if(paramparti.getModeDeJeux().getName().equalsIgnoreCase("�c�lCauchemar")) {
			
			Cauchemar cauchemar = new Cauchemar(lancement);
			cauchemar.lancementDePartie();
		}
		//Si le mode de jeux est Cauchemar est alors
		if(paramparti.getModeDeJeux().getName().equalsIgnoreCase("�c�lTonteBouftou")) {
			
			TonteBouftou tonteBouftou = new TonteBouftou(lancement);
			tonteBouftou.lancementDePartie();
		}
	
		//Si le mode de jeux est Cauchemar est alors
		if(paramparti.getModeDeJeux().getName().equalsIgnoreCase("�c�lSpeedJump")) {
			
			speedJump speedJump = new speedJump(lancement);
			speedJump.lancementDePartie();
		}
		
	}


}
