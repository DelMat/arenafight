package fr.passepartout007.gestionterrain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Location;

import fr.passepartout007.modeDeJeux.modeDejeux;

public class ArenaParam implements Serializable {

	private String name;
	private ArrayList<modeDejeux> modeDeJeux = new ArrayList<modeDejeux>();
	private ArrayList<String> locationSpawn= new ArrayList<String>();
	private ArrayList<String> locationitem= new ArrayList<String>();
	
	
	public ArenaParam(String name) {
		this.setName(name);
	}
 	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<modeDejeux> getModeDejeux() {
		return modeDeJeux;
	}
	public void addModeDeJeux(modeDejeux modeDejeux) {
		
		this.modeDeJeux.add(modeDejeux);
		
	}
	public void removeModeDeJeux(String ModeDejeux) {
		
		ArrayList<modeDejeux> mdjs = this.modeDeJeux;
		for(modeDejeux mdj :mdjs) {
			if(mdj.getName().equalsIgnoreCase(ModeDejeux)){
				this.modeDeJeux.remove(mdj);
			}
		}

	} 
	public boolean modeDeJeuxExist(String Modedejeux) {

		boolean existMode = false;
		ArrayList<modeDejeux> mdjs = this.modeDeJeux;
		System.out.println(mdjs);
		for(modeDejeux mdj :mdjs) {
			if(mdj.getName().equalsIgnoreCase(Modedejeux)) {
				existMode=true;
			}
		}
		return existMode;
		
	}
	public ArrayList<String> getLocationSpawn() {
		return locationSpawn;
	}
	public void addLocationSpawn(String locationSpawn) {
	
		this.locationSpawn.add(locationSpawn);
	}
	public void removeLocationSpawn(String locationSpawn) {
		if (locationSpawn.contains(locationSpawn)) {
			this.locationSpawn.remove(locationSpawn);
		}
		
	} 
	
	public ArrayList<String> getLocationItem() {
		return locationitem;
	}
	public void addLocationItem(String locationItem) {
	
		this.locationitem.add(locationItem);
	}
	public void removeLocationItem(String locationItem) {
		if (locationSpawn.contains(locationItem)) {
			this.locationitem.remove(locationItem);
		}
		
	} 
	public Location aleaLocationSpawn() {

		int Randomnumber = new Random().nextInt(this.locationSpawn.size());
		Arena arn = new Arena();
		
		return arn.stringToLocation(locationSpawn.get(Randomnumber));
		
	}
	
}
