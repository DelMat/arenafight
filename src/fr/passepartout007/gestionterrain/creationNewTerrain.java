package fr.passepartout007.gestionterrain;

import java.util.HashMap;
import java.util.Map.Entry;

import org.bukkit.Bukkit;



public class creationNewTerrain extends Arena {

	public creationNewTerrain() {
		super();
		}
	
	public void copieTerrain(String name) {

		String chemin = super.basededonne.CheminFichier(name + ".txt");

		HashMap<String,String> blockmap = super.importArenaData(chemin);

		System.out.println("[ArenaFight] lancement du chargement du terrain");
		for (Entry<String, String> data : blockmap.entrySet()) {		
				if(super.inArena(super.stringToLocation(data.getKey()))) {
					super.stringToLocation(data.getKey()).getBlock().setBlockData( Bukkit.createBlockData(data.getValue()));
				}
		}
					
		System.out.println("[ArenaFight] fin du chargement du terrain");

	}
}