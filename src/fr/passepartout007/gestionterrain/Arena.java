package fr.passepartout007.gestionterrain;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import fr.passepartout007.gestionbasedonnee.basededonne;
import fr.passepartout007.lancementplugin.lancement;
import fr.passepartout007.modeDeJeux.Duel;
import fr.passepartout007.modeDeJeux.modeDejeux;

public class Arena implements Listener, Serializable{
	private Integer[] sizeZ = {-1232,-1098};
	private Integer[] sizeX = {-1286,-1180};
	private Integer[] sizeY = {0,100};
	private HashMap<Integer,int[]> delimiteTerrain = new HashMap<Integer,int[]>();
	public transient lancement lancement;
	public Arena(lancement lancement){
		this.lancement = lancement;
	}
	public basededonne basededonne = new basededonne();
	public Arena( ) {
	}
	
	
	public Boolean inArena(Location location) {
		int locx = location.getBlockX();
		int locz = location.getBlockZ();
		this.setdelimiteTerrain();
	
		
		int[] locxy = delimiteTerrain.get(locz);
		if (locxy!=null) {
			if (locxy[0] <= locx && locx <= locxy[1]) {
				return true;
			}else{
				return false;
			}
		}else {
			return false;
		}
		
	}

	@EventHandler
	 public void BlockPlace(BlockPlaceEvent e) {
		Location locaBlock = e.getBlock().getLocation();
		if (!(inArena(locaBlock))) {
			e.getPlayer().sendMessage(locaBlock.getBlock().getBlockData().toString());
			///e.setCancelled(true);
		}
	}
	
	@EventHandler
	 public void BlockBreak(BlockBreakEvent e) {
		Location locaBlock = e.getBlock().getLocation();
		if (!(inArena(locaBlock))) {
			///e.setCancelled(true);
		}
	}
	
	public HashMap<String,String> importArenaData(String chemin){
		HashMap<String,String> Arenadata = new HashMap<String,String>();
		ObjectInputStream ois;
		
		try {
		      ois = new ObjectInputStream(
		              new BufferedInputStream(
		                new FileInputStream(
		                  new File(chemin))));
		      try {
		    	  Arenadata  =  (HashMap<String, String>) ois.readObject();
		    	  
		      } catch (ClassNotFoundException e) {
		          e.printStackTrace();
		        }
		  	
		        ois.close();
		} catch (FileNotFoundException e) {
	    } catch (IOException e) {
	      e.printStackTrace();
	    } 
	
		return Arenadata;
		
	}
	
	public Integer[] getSizeZ() {
		return this.sizeZ;
		
	}
	public Integer[] getSizeX() {
		return this.sizeX;
		
	}
	public Integer[] getSizeY() {
		return this.sizeY;
		
	}
	private void setdelimiteTerrain() {
		int[] tableauEntier= {-1242,-1226};
		this.delimiteTerrain.put(-1232,tableauEntier);
		tableauEntier= new int[]{-1244,-1224};	
		this.delimiteTerrain.put(-1231,tableauEntier);
		tableauEntier= new int[]{-1247,-1221};	
		this.delimiteTerrain.put(-1230,tableauEntier);
		tableauEntier= new int[]{-1250,-1218};	
		this.delimiteTerrain.put(-1229,tableauEntier);
		tableauEntier= new int[]{-1253,-1215};	
		this.delimiteTerrain.put(-1228,tableauEntier);
		tableauEntier= new int[]{-1255,-1213};	
		this.delimiteTerrain.put(-1227,tableauEntier);
		tableauEntier= new int[]{-1256,-1212};	
		this.delimiteTerrain.put(-1226,tableauEntier);
		tableauEntier= new int[]{-1259,-1209};	
		this.delimiteTerrain.put(-1225,tableauEntier);
		tableauEntier= new int[]{-1259,-1209};	
		this.delimiteTerrain.put(-1224,tableauEntier);
		tableauEntier= new int[]{-1262,-1206};	
		this.delimiteTerrain.put(-1223,tableauEntier);
		tableauEntier= new int[]{-1262,-1206};	
		this.delimiteTerrain.put(-1222,tableauEntier);
		tableauEntier= new int[]{-1264,-1204};	
		this.delimiteTerrain.put(-1221,tableauEntier);
		tableauEntier= new int[]{-1265,-1203};	
		this.delimiteTerrain.put(-1220,tableauEntier);
		tableauEntier= new int[]{-1265,-1203};	
		this.delimiteTerrain.put(-1219,tableauEntier);
		tableauEntier= new int[]{-1267,-1201};	
		this.delimiteTerrain.put(-1218,tableauEntier);
		tableauEntier= new int[]{-1268,-1200};	
		this.delimiteTerrain.put(-1217,tableauEntier);
		tableauEntier= new int[]{-1268,-1200};	
		this.delimiteTerrain.put(-1216,tableauEntier);
		tableauEntier= new int[]{-1270,-1198};	
		this.delimiteTerrain.put(-1215,tableauEntier);
		tableauEntier= new int[]{-1271,-1197};	
		this.delimiteTerrain.put(-1214,tableauEntier);
		tableauEntier= new int[]{-1271,-1197};	
		this.delimiteTerrain.put(-1213,tableauEntier);
		tableauEntier= new int[]{-1273,-1195};	
		this.delimiteTerrain.put(-1212,tableauEntier);
		tableauEntier= new int[]{-1273,-1195};	
		this.delimiteTerrain.put(-1211,tableauEntier);
		tableauEntier= new int[]{-1273,-1195};	
		this.delimiteTerrain.put(-1210,tableauEntier);
		tableauEntier= new int[]{-1275,-1193};	
		this.delimiteTerrain.put(-1209,tableauEntier);
		tableauEntier= new int[]{-1275,-1193};	
		this.delimiteTerrain.put(-1208,tableauEntier);
		tableauEntier= new int[]{-1275,-1193};	
		this.delimiteTerrain.put(-1207,tableauEntier);
		tableauEntier= new int[]{-1277,-1191};	
		this.delimiteTerrain.put(-1206,tableauEntier);
		tableauEntier= new int[]{-1277,-1191};	
		this.delimiteTerrain.put(-1205,tableauEntier);
		tableauEntier= new int[]{-1277,-1191};	
		this.delimiteTerrain.put(-1204,tableauEntier);
		tableauEntier= new int[]{-1278,-1190};	
		this.delimiteTerrain.put(-1203,tableauEntier);
		tableauEntier= new int[]{-1279,-1189};	
		this.delimiteTerrain.put(-1202,tableauEntier);
		tableauEntier= new int[]{-1278,-1190};	
		this.delimiteTerrain.put(-1201,tableauEntier);
		tableauEntier= new int[]{-1280,-1188};	
		this.delimiteTerrain.put(-1200,tableauEntier);
		tableauEntier= new int[]{-1280,-1188};	
		this.delimiteTerrain.put(-1199,tableauEntier);
		tableauEntier= new int[]{-1280,-1188};	
		this.delimiteTerrain.put(-1198,tableauEntier);
		tableauEntier= new int[]{-1281,-1187};	
		this.delimiteTerrain.put(-1197,tableauEntier);
		tableauEntier= new int[]{-1282,-1186};	
		this.delimiteTerrain.put(-1196,tableauEntier);
		tableauEntier= new int[]{-1281,-1187};	
		this.delimiteTerrain.put(-1195,tableauEntier);
		tableauEntier= new int[]{-1282,-1186};	
		this.delimiteTerrain.put(-1194,tableauEntier);
		tableauEntier= new int[]{-1283,-1185};	
		this.delimiteTerrain.put(-1193,tableauEntier);
		tableauEntier= new int[]{-1282,-1186};	
		this.delimiteTerrain.put(-1192,tableauEntier);
		tableauEntier= new int[]{-1283,-1185};	
		this.delimiteTerrain.put(-1191,tableauEntier);
		tableauEntier= new int[]{-1284,-1184};	
		this.delimiteTerrain.put(-1190,tableauEntier);
		tableauEntier= new int[]{-1283,-1185};	
		this.delimiteTerrain.put(-1189,tableauEntier);
		tableauEntier= new int[]{-1284,-1184};	
		this.delimiteTerrain.put(-1188,tableauEntier);
		tableauEntier= new int[]{-1284,-1184};	
		this.delimiteTerrain.put(-1187,tableauEntier);
		tableauEntier= new int[]{-1284,-1184};	
		this.delimiteTerrain.put(-1186,tableauEntier);
		tableauEntier= new int[]{-1285,-1183};	
		this.delimiteTerrain.put(-1185,tableauEntier);
		tableauEntier= new int[]{-1285,-1183};	
		this.delimiteTerrain.put(-1184,tableauEntier);
		tableauEntier= new int[]{-1284,-1184};	
		this.delimiteTerrain.put(-1183,tableauEntier);
		tableauEntier= new int[]{-1286,-1182};	
		this.delimiteTerrain.put(-1182,tableauEntier);
		tableauEntier= new int[]{-1286,-1182};	
		this.delimiteTerrain.put(-1181,tableauEntier);
		tableauEntier= new int[]{-1285,-1183};	
		this.delimiteTerrain.put(-1180,tableauEntier);
		tableauEntier= new int[]{-1286,-1182};	
		this.delimiteTerrain.put(-1179,tableauEntier);
		tableauEntier= new int[]{-1286,-1182};	
		this.delimiteTerrain.put(-1178,tableauEntier);
		tableauEntier= new int[]{-1285,-1183};	
		this.delimiteTerrain.put(-1177,tableauEntier);
		tableauEntier= new int[]{-1285,-1180};	
		this.delimiteTerrain.put(-1176,tableauEntier);
		tableauEntier= new int[]{-1285,-1180};	
		this.delimiteTerrain.put(-1175,tableauEntier);
		tableauEntier= new int[]{-1285,-1180};	
		this.delimiteTerrain.put(-1174,tableauEntier);
		tableauEntier= new int[]{-1285,-1180};	
		this.delimiteTerrain.put(-1173,tableauEntier);
		tableauEntier= new int[]{-1285,-1180};	
		this.delimiteTerrain.put(-1172,tableauEntier);
		tableauEntier= new int[]{-1285,-1180};	
		this.delimiteTerrain.put(-1171,tableauEntier);
		tableauEntier= new int[]{-1285,-1180};	
		this.delimiteTerrain.put(-1170,tableauEntier);
		tableauEntier= new int[]{-1285,-1180};	
		this.delimiteTerrain.put(-1169,tableauEntier);
		tableauEntier= new int[]{-1285,-1180};	
		this.delimiteTerrain.put(-1168,tableauEntier);
		tableauEntier= new int[]{-1285,-1180};	
		this.delimiteTerrain.put(-1167,tableauEntier);
		tableauEntier= new int[]{-1285,-1180};	
		this.delimiteTerrain.put(-1166,tableauEntier);
		tableauEntier= new int[]{-1285,-1180};	
		this.delimiteTerrain.put(-1165,tableauEntier);
		tableauEntier= new int[]{-1285,-1180};	
		this.delimiteTerrain.put(-1164,tableauEntier);
		tableauEntier= new int[]{-1285,-1180};	
		this.delimiteTerrain.put(-1163,tableauEntier);
		tableauEntier= new int[]{-1285,-1180};	
		this.delimiteTerrain.put(-1162,tableauEntier);
		tableauEntier= new int[]{-1285,-1180};	
		this.delimiteTerrain.put(-1161,tableauEntier);
		tableauEntier= new int[]{-1285,-1180};	
		this.delimiteTerrain.put(-1160,tableauEntier);
		tableauEntier= new int[]{-1285,-1180};	
		this.delimiteTerrain.put(-1159,tableauEntier);
		tableauEntier= new int[]{-1285,-1180};	
		this.delimiteTerrain.put(-1158,tableauEntier);
		tableauEntier= new int[]{-1285,-1180};	
		this.delimiteTerrain.put(-1157,tableauEntier);
		tableauEntier= new int[]{-1285,-1180};	
		this.delimiteTerrain.put(-1156,tableauEntier);
		tableauEntier= new int[]{-1285,-1183};	
		this.delimiteTerrain.put(-1155,tableauEntier);
		tableauEntier= new int[]{-1286,-1182};	
		this.delimiteTerrain.put(-1154,tableauEntier);
		tableauEntier= new int[]{-1286,-1182};	
		this.delimiteTerrain.put(-1153,tableauEntier);
		tableauEntier= new int[]{-1285,-1183};	
		this.delimiteTerrain.put(-1152,tableauEntier);
		tableauEntier= new int[]{-1286,-1182};	
		this.delimiteTerrain.put(-1151,tableauEntier);
		tableauEntier= new int[]{-1286,-1182};	
		this.delimiteTerrain.put(-1150,tableauEntier);
		tableauEntier= new int[]{-1284,-1184};	
		this.delimiteTerrain.put(-1149,tableauEntier);
		tableauEntier= new int[]{-1285,-1183};	
		this.delimiteTerrain.put(-1148,tableauEntier);
		tableauEntier= new int[]{-1285,-1183};	
		this.delimiteTerrain.put(-1147,tableauEntier);
		tableauEntier= new int[]{-1284,-1184};	
		this.delimiteTerrain.put(-1146,tableauEntier);
		tableauEntier= new int[]{-1284,-1184};	
		this.delimiteTerrain.put(-1145,tableauEntier);
		tableauEntier= new int[]{-1284,-1184};	
		this.delimiteTerrain.put(-1144,tableauEntier);
		tableauEntier= new int[]{-1283,-1185};	
		this.delimiteTerrain.put(-1143,tableauEntier);
		tableauEntier= new int[]{-1284,-1184};	
		this.delimiteTerrain.put(-1142,tableauEntier);
		tableauEntier= new int[]{-1283,-1185};	
		this.delimiteTerrain.put(-1141,tableauEntier);
		tableauEntier= new int[]{-1282,-1186};	
		this.delimiteTerrain.put(-1140,tableauEntier);
		tableauEntier= new int[]{-1283,-1185};	
		this.delimiteTerrain.put(-1139,tableauEntier);
		tableauEntier= new int[]{-1282,-1186};	
		this.delimiteTerrain.put(-1138,tableauEntier);
		tableauEntier= new int[]{-1281,-1187};	
		this.delimiteTerrain.put(-1137,tableauEntier);
		tableauEntier= new int[]{-1282,-1186};	
		this.delimiteTerrain.put(-1136,tableauEntier);
		tableauEntier= new int[]{-1281,-1187};	
		this.delimiteTerrain.put(-1135,tableauEntier);
		tableauEntier= new int[]{-1280,-1188};	
		this.delimiteTerrain.put(-1134,tableauEntier);
		tableauEntier= new int[]{-1280,-1188};	
		this.delimiteTerrain.put(-1133,tableauEntier);
		tableauEntier= new int[]{-1280,-1188};	
		this.delimiteTerrain.put(-1132,tableauEntier);
		tableauEntier= new int[]{-1278,-1190};	
		this.delimiteTerrain.put(-1131,tableauEntier);
		tableauEntier= new int[]{-1279,-1189};	
		this.delimiteTerrain.put(-1130,tableauEntier);
		tableauEntier= new int[]{-1278,-1190};	
		this.delimiteTerrain.put(-1129,tableauEntier);
		tableauEntier= new int[]{-1277,-1191};	
		this.delimiteTerrain.put(-1128,tableauEntier);
		tableauEntier= new int[]{-1277,-1191};	
		this.delimiteTerrain.put(-1127,tableauEntier);
		tableauEntier= new int[]{-1277,-1191};	
		this.delimiteTerrain.put(-1126,tableauEntier);
		tableauEntier= new int[]{-1275,-1193};	
		this.delimiteTerrain.put(-1125,tableauEntier);
		tableauEntier= new int[]{-1275,-1193};	
		this.delimiteTerrain.put(-1124,tableauEntier);
		tableauEntier= new int[]{-1275,-1193};	
		this.delimiteTerrain.put(-1123,tableauEntier);
		tableauEntier= new int[]{-1273,-1195};	
		this.delimiteTerrain.put(-1122,tableauEntier);
		tableauEntier= new int[]{-1273,-1195};	
		this.delimiteTerrain.put(-1121,tableauEntier);
		tableauEntier= new int[]{-1273,-1195};	
		this.delimiteTerrain.put(-1120,tableauEntier);
		tableauEntier= new int[]{-1271,-1197};	
		this.delimiteTerrain.put(-1119,tableauEntier);
		tableauEntier= new int[]{-1271,-1197};	
		this.delimiteTerrain.put(-1118,tableauEntier);
		tableauEntier= new int[]{-1270,-1198};	
		this.delimiteTerrain.put(-1117,tableauEntier);
		tableauEntier= new int[]{-1268,-1200};	
		this.delimiteTerrain.put(-1116,tableauEntier);
		tableauEntier= new int[]{-1268,-1200};	
		this.delimiteTerrain.put(-1115,tableauEntier);
		tableauEntier= new int[]{-1267,-1201};	
		this.delimiteTerrain.put(-1114,tableauEntier);
		tableauEntier= new int[]{-1265,-1203};	
		this.delimiteTerrain.put(-1113,tableauEntier);
		tableauEntier= new int[]{-1265,-1203};	
		this.delimiteTerrain.put(-1112,tableauEntier);
		tableauEntier= new int[]{-1264,-1204};	
		this.delimiteTerrain.put(-1111,tableauEntier);
		tableauEntier= new int[]{-1262,-1206};	
		this.delimiteTerrain.put(-1110,tableauEntier);
		tableauEntier= new int[]{-1262,-1206};	
		this.delimiteTerrain.put(-1109,tableauEntier);
		tableauEntier= new int[]{-1259,-1209};	
		this.delimiteTerrain.put(-1108,tableauEntier);
		tableauEntier= new int[]{-1259,-1209};	
		this.delimiteTerrain.put(-1107,tableauEntier);
		tableauEntier= new int[]{-1256,-1212};	
		this.delimiteTerrain.put(-1106,tableauEntier);
		tableauEntier= new int[]{-1255,-1213};	
		this.delimiteTerrain.put(-1105,tableauEntier);
		tableauEntier= new int[]{-1253,-1215};	
		this.delimiteTerrain.put(-1104,tableauEntier);
		tableauEntier= new int[]{-1250,-1218};	
		this.delimiteTerrain.put(-1103,tableauEntier);
		tableauEntier= new int[]{-1247,-1221};	
		this.delimiteTerrain.put(-1102,tableauEntier);
		tableauEntier= new int[]{-1244,-1224};	
		this.delimiteTerrain.put(-1101,tableauEntier);
		tableauEntier= new int[]{-1242,-1226};	
		this.delimiteTerrain.put(-1100,tableauEntier);
		tableauEntier= new int[]{-1242,-1226};	
		this.delimiteTerrain.put(-1099,tableauEntier);
		tableauEntier= new int[]{-1242,-1225};	
		this.delimiteTerrain.put(-1098,tableauEntier);
		
		
	}

	public String locattostring(Location location) {
		String locations =location.getBlockX() + "/" + location.getBlockY() +"/"+ location.getBlockZ()+ "/" + location.getWorld().getName();
		return locations;
	}
	
	public Location stringToLocation(String location) {
		String[] partloc = location.split("/");
		Location locations = new Location(Bukkit.getServer().getWorld(partloc[3]), Integer.parseInt(partloc[0]), Integer.parseInt(partloc[1]), Integer.parseInt(partloc[2]));
		return locations;
	}
	
	public ArrayList<ArenaParam> importArenaParameterList(){

		
		ArrayList<ArenaParam>  listArene = new ArrayList<ArenaParam>();
		basededonne bdd = new basededonne();
		String chemin = "ListAreneParam.txt";
		chemin = bdd.CheminFichier(chemin);
		ObjectInputStream ois;
		
		try {
		      ois = new ObjectInputStream(
		              new BufferedInputStream(
		                new FileInputStream(
		                  new File(chemin))));
		      try {
		    	  listArene  =  (ArrayList<ArenaParam> ) ois.readObject();
		    	  
		      } catch (ClassNotFoundException e) {
		          e.printStackTrace();
		        }
		  	
		        ois.close();
		} catch (FileNotFoundException e) {
	    } catch (IOException e) {
	      e.printStackTrace();
	    } 
	
		return listArene;
		
	}
	public ArenaParam importArenaParameter(String str){

		
		ArrayList<ArenaParam>  listArene = new ArrayList<ArenaParam>();
		basededonne bdd = new basededonne();
		String chemin = "ListAreneParam.txt";
		chemin = bdd.CheminFichier(chemin);
		ObjectInputStream ois;

		try {
		      ois = new ObjectInputStream(
		              new BufferedInputStream(
		                new FileInputStream(
		                  new File(chemin))));
		      try {
		    	  listArene  =  (ArrayList<ArenaParam> ) ois.readObject();
		    	  
		      } catch (ClassNotFoundException e) {
		          e.printStackTrace();
		        }
		  	
		        ois.close();
		} catch (FileNotFoundException e) {
	    } catch (IOException e) {
	      e.printStackTrace();
	    } 
		
		for(ArenaParam arnParam : listArene) {

			if(arnParam.getName().equalsIgnoreCase(str)) {
				return arnParam;
			}
		}
		
		return null;
		
	}

	public void listArenePlayer(Player player) {
		ArrayList<ArenaParam>  listarene = importArenaParameterList();
		player.sendMessage("�l�eLa liste des arenes est :");
		for(ArenaParam arene : listarene) {
			player.sendMessage("�b" + arene.getName());
		}
	}

	public ArrayList<String> ArenaListName(){
		ArrayList<String> ArenaNameList = new ArrayList<String>();
		ArrayList<ArenaParam> arenaParam = importArenaParameterList();
		
		for(ArenaParam arena: arenaParam) {
		 
			ArenaNameList.add(arena.getName());
		}
		
		return ArenaNameList;
		
	}

	public void supprimerArena(String name, Player player) {
		player.sendMessage("�l�eLancement de la suppression d\'ar�ne");
		basededonne bdd = new basededonne();
		SaveNewArena sna = new SaveNewArena();
		 ArrayList<ArenaParam> paramArenaList = importArenaParameterList();
		 ArrayList<ArenaParam> paramArenaListnew = new ArrayList<ArenaParam>();
		 for(ArenaParam arenaParam: paramArenaList) {
			 if(!arenaParam.getName().equalsIgnoreCase(name)) {
				 paramArenaListnew.add(arenaParam);
				 			
			 }
		 }
		 sna.exportArenaParam(paramArenaListnew);
		 
		 player.sendMessage("�l�eL\'ar�ne�b " + name + "�l�e a �t� supprimer de la liste des ar�nes");
		try{
			 File file = new File(bdd.CheminFichier(name) + ".txt");

			 if (file.delete()) {
				player.sendMessage("�l�eLe fichier de l\'ar�ne�b " + name + "�l�e a �t� supprimer"); 
			 }
	     }catch(Exception e){
	         e.printStackTrace();
	     }
		     
	}

	public void addLocationtoArena(String name, Location location,Player player) {
		ArrayList<ArenaParam> listarenaParameter = importArenaParameterList();
		int index = 0;
		boolean traiter = false;

		for(ArenaParam arenaParameter : listarenaParameter) {

			if(arenaParameter.getName().equalsIgnoreCase(name)) {
				
				if(arenaParameter.getLocationSpawn().contains(this.locattostring(location)) != true){

					arenaParameter.addLocationSpawn(this.locattostring(location));

					ArenaParam newArenaparameter = new ArenaParam(name);

					newArenaparameter = arenaParameter;

					listarenaParameter.remove(index);
	
					listarenaParameter.add(newArenaparameter);
	
					traiter = true;
			
				}else{
		
					player.sendMessage("�b La position existe d�j� dans cette ar�ne.");
				}
				
			}

			index++;
	
		}

		SaveNewArena sna = new SaveNewArena();
		sna.exportArenaParam(listarenaParameter);
		
		if (traiter == false) {
			player.sendMessage("�b une erreur est survenue la position n'as pas �t� ajout�e.");
		}else {
			player.sendMessage("�b La position de spawn � bien �t� ajout�e.");
		}
	}

	public void supprLocationToArena(String name, Location location, Player player) {
		ArrayList<ArenaParam> listarenaParameter = importArenaParameterList();
		int index = 0;
		
		boolean traiter = false;
		for(ArenaParam arenaParameter : listarenaParameter) {
			
			if(arenaParameter.getName().equalsIgnoreCase(name)) {
				if(arenaParameter.getLocationSpawn().contains(this.locattostring(location))){
				
					arenaParameter.removeLocationSpawn(this.locattostring(location));
					ArenaParam newArenaparameter = arenaParameter;
					listarenaParameter.remove(index);
					listarenaParameter.add(newArenaparameter);
					traiter = true;
				}else{
					player.sendMessage("�b La position de spawn n'existe pas dans cette ar�ne.");
				}
			}		
			index++;
		}
		

		SaveNewArena sna = new SaveNewArena();
		sna.exportArenaParam(listarenaParameter);
		
		if (traiter == false) {
			player.sendMessage("�b une erreur est survenue la position n'as pas �t� supprim�e.");
		}else {
			player.sendMessage("�b La position de spawn � bien �t� supprim�e.");
		}
	}
	
	public void addModedeJeuxtoArena(String name, String Modedejeux, Player player) {
		
		ArrayList<ArenaParam> listarenaParameter = importArenaParameterList();


		boolean traiter = false;

		for(ArenaParam arenaParameter : listarenaParameter) {

	
			if(arenaParameter.getName().equalsIgnoreCase(name)) {

		
				boolean modedejeuxexist = arenaParameter.modeDeJeuxExist("�c�l"+Modedejeux);
	
				if(modedejeuxexist == false) {
					modeDejeux mdjs = new Duel(lancement);
					mdjs.setListModedeJeux();
				
					modeDejeux modeDejeux = mdjs.getModedejeux(Modedejeux);
					
							arenaParameter.addModeDeJeux(modeDejeux);
		
							ArenaParam newArenaparameter = arenaParameter;
	
							listarenaParameter.remove(arenaParameter);
			
							listarenaParameter.add(newArenaparameter);
		
							traiter = true;
		
							break;
				
					
				}else {

					player.sendMessage("�b Le mode de jeux exist d�j� pour cette ar�ne");
			
				}
			}
	
	
		}

		SaveNewArena sna = new SaveNewArena();
		sna.exportArenaParam(listarenaParameter);
		
		if (traiter == false) {
			player.sendMessage("�b une erreur est survenue le mode de jeux n'as pas �t� ajout�e.");
		}else {
			player.sendMessage("�b Le mode de jeux � bien �t� ajout�e.");
		}	
	}
	
	public void supprModeDeJeuxtoArena(String name, String Modedejeux, Player player) {
		ArrayList<ArenaParam> listarenaParameter = importArenaParameterList();
		int index0 = 0;
		boolean traiter = false;
		for(ArenaParam arenaParameter : listarenaParameter) {

			if(arenaParameter.getName().equalsIgnoreCase(name)) {

				if(arenaParameter.modeDeJeuxExist("�c�l"+Modedejeux)) {					
					arenaParameter.removeModeDeJeux("�c�l"+Modedejeux);
					ArenaParam newArenaparameter = arenaParameter;
					listarenaParameter.remove(index0);
					listarenaParameter.add(newArenaparameter);
					traiter = true;					
				}else {					
					player.sendMessage("�bLe mode de jeux n'existe pas pour cette ar�ne");;
				}
				
			}
		index0++;
		}
		
		SaveNewArena sna = new SaveNewArena();
		sna.exportArenaParam(listarenaParameter);
		if (traiter == false) {
			player.sendMessage("�b une erreur est survenue le mode de jeux n'as pas �t� supprim�.");
		}else {
			player.sendMessage("�b Le mode de jeux � bien �t� supprim�.");
		}	
	}
	
}