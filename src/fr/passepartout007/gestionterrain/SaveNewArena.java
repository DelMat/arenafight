package fr.passepartout007.gestionterrain;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import fr.passepartout007.gestionbasedonnee.basededonne;

public class SaveNewArena extends Arena{

	public SaveNewArena( ) {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public void exportNewArena(Player player, String name) {
		World world = player.getWorld();
		player.sendMessage("�bSauvegarde de la map");
		HashMap<String,String> saveBlock = new HashMap<String,String>();
		//boucleZ
		for(int i= super.getSizeZ()[0] ; i<super.getSizeZ()[1] ; i++) {
			
				player.sendMessage("�bIl reste : �f" + (((i*-1)-1098) + " couches"));
			
			
			//bouclex
			for(int j= super.getSizeX()[0] ; j <super.getSizeX()[1]+1 ; j++) {
				for(int k=super.getSizeY()[0] ; k<super.getSizeY()[1]+1 ;k++) {
					Location location = new Location(world, j, k, i);
						String data = location.getBlock().getBlockData().getAsString();
						saveBlock.put(super.locattostring(location),data);
				}
			}
				
		}
		basededonne bdd = new basededonne();
		String chemin = name +".txt";
		chemin = bdd.CheminFichier(chemin);
		
		
		
		ArrayList<ArenaParam> ArenalistParam = (ArrayList<ArenaParam>) importArenaParameterList();

		if( ArenalistParam == null) {
			ArenalistParam = new ArrayList<ArenaParam>();
			ArenalistParam.add(new ArenaParam(name));
			exportArenaData(chemin,saveBlock);
			exportArenaParam(ArenalistParam);
			player.sendMessage("�bSauvegarde Fini");
		}else {
			Boolean arenaExist =false;
			for(ArenaParam arenaParam:ArenalistParam) {
				if (arenaParam.getName().equalsIgnoreCase(name)) {
					arenaExist =true;
				}							
			}
			if(!arenaExist) {
				ArenalistParam.add(new ArenaParam(name));
				exportArenaData(chemin,saveBlock);
				exportArenaParam(ArenalistParam);
				player.sendMessage("�bSauvegarde Fini");
			}else {
				player.sendMessage("�b L'arene existe d�ja, merci de la supprimer avant de la remplacer");
			}

		}
		
		
		
	}
	
	public void exportArenaData(String chemin,HashMap<String,String> saveBlock ) {
		ObjectOutputStream oos;
		try {
			
			oos = new ObjectOutputStream(
		              new BufferedOutputStream(
		                      new FileOutputStream(
		                        new File(chemin))));
			
			oos.writeObject(saveBlock);
			oos.close();
		
		} catch (FileNotFoundException e) {
		      e.printStackTrace();
	    } catch (IOException e) {
	      e.printStackTrace();
	    }   
	}
	
	public void exportArenaParam(ArrayList<ArenaParam> ArenalistParam) {
		basededonne bdd = new basededonne();
		String chemin = "ListAreneParam.txt";
		chemin = bdd.CheminFichier(chemin);
	
		ObjectOutputStream oos;
		try {
			
			oos = new ObjectOutputStream(
		              new BufferedOutputStream(
		                      new FileOutputStream(
		                        new File(chemin))));
			
			oos.writeObject(ArenalistParam);
			oos.close();
		
		} catch (FileNotFoundException e) {
		      e.printStackTrace();
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	}

}
