package fr.passepartout007.lancementplugin;

import java.io.Serializable;
import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Sheep;
import org.bukkit.plugin.java.JavaPlugin;

import fr.passepartout007.commands.Commandes;
import fr.passepartout007.gestionbasedonnee.ListdesJoueur;
import fr.passepartout007.gestionbasedonnee.basededonne;
import fr.passepartout007.gestionjoueur.ConnectionDeconnectionJoueur;
import fr.passepartout007.gestionjoueur.Joueur;
import fr.passepartout007.gestionjoueur.Mode;
import fr.passepartout007.gestionterrain.Arena;
import fr.passepartout007.modeDeJeux.Cauchemar;
import fr.passepartout007.modeDeJeux.DesACoudre;
import fr.passepartout007.modeDeJeux.Duel;
import fr.passepartout007.modeDeJeux.FloconFall;
import fr.passepartout007.modeDeJeux.NoMode;
import fr.passepartout007.modeDeJeux.TonteBouftou;
import fr.passepartout007.modeDeJeux.speedJump;
import fr.passepartout007.phasedujeux.ParametreDePartie;
import fr.passepartout007.phasedujeux.attenteduJeux;
import fr.passepartout007.scoreboard.GestionScoreboard;



public class lancement extends JavaPlugin implements Serializable{
	private static transient lancement instance;

	
    private void setInstance(lancement instance) {
        this.instance = instance;
    }
    public static lancement getInstance() {
        return instance;
    }
	public void IntialisationJoueur() {



	
		Collection<? extends Player> listPlayerOnline =  Bukkit.getOnlinePlayers();
		for(Player player : listPlayerOnline) {
	

			 
			
			 attenteduJeux attjeux = new attenteduJeux(this);
			 player.teleport(attjeux.getLoctionSpawn(player.getWorld()));
			 player.setGameMode(GameMode.ADVENTURE);
			 player.setFireTicks(0);
			 player.setHealth(20);
			 player.setSaturation(20);
			 player.getInventory().clear();
		}
	}


	@Override
	public void onEnable() {
		this.setInstance(this);
		System.out.println("-----------------------------");
		System.out.println("Initialisation du plugin ArenaFight");
		System.out.println("-----------------------------");
		//initialise les commande du jeux
		getCommand("ExportNewArena").setExecutor(new Commandes());
		getCommand("SupprimerArena").setExecutor(new Commandes());
		getCommand("addLocationToArena").setExecutor(new Commandes());
		getCommand("supprLocationToArena").setExecutor(new Commandes());
		getCommand("addModedeJeuxtoArena").setExecutor(new Commandes());
		getCommand("supprModedeJeuxtoArena").setExecutor(new Commandes());
		getCommand("LoadArena").setExecutor(new Commandes());
		
		
		getServer().getPluginManager().registerEvents(new speedJump(this), this);
		getServer().getPluginManager().registerEvents(new DesACoudre(this), this);
		getServer().getPluginManager().registerEvents(new TonteBouftou(this), this);
		getServer().getPluginManager().registerEvents(new NoMode(this), this);
		getServer().getPluginManager().registerEvents(new FloconFall(this), this);
		getServer().getPluginManager().registerEvents(new Duel(this), this);
		getServer().getPluginManager().registerEvents(new Cauchemar(this), this);
		getServer().getPluginManager().registerEvents(new attenteduJeux(this), this);
		getServer().getPluginManager().registerEvents(new ConnectionDeconnectionJoueur(this), this);
		getServer().getPluginManager().registerEvents(new Arena(this), this);
		
		


		


	        this.getConfig().options().copyDefaults(true);
	        this.saveConfig();

		System.out.println("[ArenaFigh] Exportation des donn�es de partie");
		ParametreDePartie pdp = new ParametreDePartie();
		ListdesJoueur listJoueur = new ListdesJoueur();
		NoMode noMode = new NoMode(this);
		pdp.setModeDeJeux(noMode);
		pdp.setEncours(false);
		pdp.setListJoueur(listJoueur);

		System.out.println("[ArenaFigh] Export des donn�es de partie fini");

		System.out.println("[ArenaFigh] Exportation des donn�es des joueur");
	
		Collection<? extends Player> listPlayerOnline =  Bukkit.getOnlinePlayers();
		if(listPlayerOnline!=null) {
			for(Player onlinePlayer : listPlayerOnline) {
				Joueur Joueuronline = new Joueur(onlinePlayer.getName(), Mode.JOUEUR);
				listJoueur.addJoueurtolistSansImport((Joueuronline));
	

			
			}
		}
		for(World world : Bukkit.getServer().getWorlds()){
	        for(Entity entity : world.getEntities()){
	            if(entity instanceof Sheep){
	            	entity.remove();
	            }
	        }
		}
		IntialisationJoueur();
		listJoueur.exportListOfJoueur();
		pdp.setListJoueur(listJoueur);
		basededonne bdd = new basededonne();
		bdd.exportParametrePartie(pdp);
		System.out.println("[ArenaFigh] Export des donn�es des joueurFini");
		System.out.println("[ArenaFigh] Mise � jours des Scoreboard");
		GestionScoreboard gs = new GestionScoreboard();
		gs.MajScoreboardJeux(this);
		System.out.println("[ArenaFigh] Mise � jours des Scoreboard Fini");

		System.out.println("-----------------------------");
		System.out.println("Fin de l'initialisation du plugin ArenaFight");
		System.out.println("-----------------------------");
		

	}
	

}
