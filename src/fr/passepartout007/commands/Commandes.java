package fr.passepartout007.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.passepartout007.gestionterrain.Arena;
import fr.passepartout007.gestionterrain.SaveNewArena;
import fr.passepartout007.gestionterrain.creationNewTerrain;

public class Commandes implements CommandExecutor {

	
	public Commandes () {
		

	}
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String msg, String[] arg) {
		
		if(sender instanceof Player) {
			Player player = (Player)sender;
			

			if(msg.equalsIgnoreCase("ExportNewArena")) {
				if( arg.length != 1) {
					player.sendMessage("�bLa commande ExportNewArena s'�crit : �f/ExportNewArena <Nom de l'arene>");
					return false;
				}
				SaveNewArena arene = new SaveNewArena();
				arene.exportNewArena(player, arg[0]);
				return false;
			}
			if(msg.equalsIgnoreCase("LoadArena")) {
				if( arg.length != 1) {
					player.sendMessage("�bLa commande SupprimerArena s'�crit : �f/SupprimerArena <Nom de l'arene>");
					return false;
				}
				creationNewTerrain creationNewTerrain = new creationNewTerrain();
				creationNewTerrain.copieTerrain(arg[0]);
				return false;
			}
			if(msg.equalsIgnoreCase("SupprimerArena")) {
				if( arg.length != 1) {
					player.sendMessage("�bLa commande SupprimerArena s'�crit : �f/SupprimerArena <Nom de l'arene>");
					return false;
				}
				Arena arene = new Arena();
				arene.supprimerArena(arg[0], player);
				return false;
			}
			
			if(msg.equalsIgnoreCase("addModedeJeuxtoArena")) {
				
				if( arg.length != 2) {
					
					player.sendMessage("�bLa commande addModedeJeuxtoArena s'�crit : �f/addModedeJeuxtoArena <Nom de l'arene> <Nom du mode de jeux>");
					return false;
				}
				Arena arn = new Arena();
				arn.addModedeJeuxtoArena(arg[0], arg[1], player);
				
			}
			if(msg.equalsIgnoreCase("supprModedeJeuxtoArena")) {
				if( arg.length != 2 ) {
					player.sendMessage("�bLa commande supprModedeJeuxtoArena s'�crit : �f/supprModedeJeuxtoArena <Nom de l'arene> <Nom du mode de jeux>");
					return false;
				}else {
					Arena arn = new Arena();
					arn.supprModeDeJeuxtoArena(arg[0], arg[1], player);
				}
			}
			
			if(msg.equalsIgnoreCase("addLocationToArena")) {
				if( arg.length !=1) {
					player.sendMessage("�bLa commande addLocationToArena s'�crit : �f/addLocationToArena <Nom de l'arene> de �bet prend la position de votre joueur" );
					return false;
				}else {
					Arena arn = new Arena();
					arn.addLocationtoArena(arg[0], player.getLocation(), player);
				}
				
			}
			
			if(msg.equalsIgnoreCase("supprLocationToArena")) {
				if( arg.length !=1) {
					player.sendMessage("�bLa commande supprLocationToArena s'�crit : �f/supprLocationToArena <Nom de l'arene> �bet prend la position de votre joueur" );
					return false;
				}else {
					Arena arn = new Arena();
					arn.supprLocationToArena(arg[0], player.getLocation(), player);
				}
				
			}
			
		}
		return false;
	}

}
