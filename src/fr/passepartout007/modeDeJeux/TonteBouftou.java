package fr.passepartout007.modeDeJeux;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Random;
import java.util.TreeMap;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Sheep;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import fr.passepartout007.gestionbasedonnee.ListdesJoueur;
import fr.passepartout007.gestionbasedonnee.basededonne;
import fr.passepartout007.gestionjoueur.Joueur;
import fr.passepartout007.gestionjoueur.Mode;
import fr.passepartout007.gestionjoueur.gestionSkin.MakeSkin;
import fr.passepartout007.gestionjoueur.gestionSkin.Skins;
import fr.passepartout007.gestionterrain.ArenaParam;
import fr.passepartout007.gestionterrain.creationNewTerrain;
import fr.passepartout007.lancementplugin.lancement;
import fr.passepartout007.phasedujeux.ParametreDePartie;
import fr.passepartout007.phasedujeux.attenteduJeux;
import fr.passepartout007.scoreboard.GestionScoreboard;
import fr.passepartout007.scoreboard.ScoreBoards;
import fr.passepartout007.scoreboard.ScoreboardCauchemar;
import fr.passepartout007.scoreboard.ScoreboardTonteBouftou;


public class TonteBouftou extends modeDejeux implements Serializable, Listener{
	public ScoreboardTonteBouftou Scoarboard= new ScoreboardTonteBouftou();
	public int timer =120;
	public TonteBouftou(lancement lancement) {
		super(lancement);
		super.setName("�c�lTonteBouftou");
		super.setDescription( "�bLa saison annuel de la tonte des Bouftous est arriv�e, "
				+ "attaper un maximum de boufton et tondez les avec votre cisaille. "
				+ "L�ensemble des joueurs sont t�l�port�s al�atoirement dans l�ar�ne, "
				+ "une laine de bouftou blanc rapporte 1 points. "
				+ "une laine de bouftou noir rapporte 2 points. "
				+ "une laine de bouftou rose rapporte 3 points. "
				+ "une laine de bouftou jaune rapporte 5 points. "
				+ "la partie dure 2 minutes. "
				+ "Pour gagner il faut avoir le plus grand score.");
	}
	
	@Override
	public ScoreBoards getScorboard() {
		return this.Scoarboard;
	}
	
public void lancementDePartie() {
		GestionScoreboard gs = new GestionScoreboard();
		
		basededonne bdd = new basededonne();
		
		ParametreDePartie paramPartie = bdd.importParametrePartie();
	
		paramPartie.setEncours(true);
		bdd.exportParametrePartie(paramPartie);
		creationNewTerrain creationNewTerrain  =new creationNewTerrain();
		creationNewTerrain.copieTerrain(paramPartie.getArene().getName());
		
		Collection<? extends Player> listPlayerOnline =  Bukkit.getOnlinePlayers();
		ListdesJoueur listJoueur = paramPartie.getListJoueur();

		for(Player player :listPlayerOnline) {
			if(listJoueur.PlayerExist(player)) {
				Joueur joueur = listJoueur.getJoueur(player);
				if(joueur.getMode() == Mode.JOUEUR) {

					
					joueur.setVie(100000);
					joueur.setScore(0);
					
					listJoueur.suppJoueurtolist(joueur);
					listJoueur.addJoueurtolist(joueur);
					paramPartie.setListJoueur(listJoueur);
					bdd.exportParametrePartie(paramPartie);
					teleporteJoueur(player);
			

					
					this.resetRoundJoueur(player);
					
					
					
				}else if(joueur.getMode() == Mode.SPECTATEUR) {

					teleporteSpectateur(player);

				}
			}else {

				teleporteSpectateur(player);

			}
		}
		spawnSheepAl�atoire();
		gs.MajScoreboardJeux(lancement);
	}

	public void teleporteSpectateur(Player player) {
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();

		player.teleport(paramPartie.getArene().aleaLocationSpawn());
		player.getWorld().setSpawnLocation(player.getLocation());
		player.setGameMode(GameMode.SPECTATOR);
		
	}
	
	public void teleporteJoueur(Player player) {
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();

		player.teleport(paramPartie.getArene().aleaLocationSpawn());
	
	
	}
	


	public void resetRoundJoueur(Player player) {
		

		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();


		ScoreboardCauchemar scd = new ScoreboardCauchemar();

		
		
		player.setGameMode(GameMode.SURVIVAL);
		this.teleporteJoueur(player);
		player.getWorld().setSpawnLocation(player.getLocation());
		player.getInventory().clear();

		player.getInventory().addItem(new ItemStack(Material.SHEARS));

		player.addPotionEffect(PotionEffectType.FIRE_RESISTANCE.createEffect(100, 3));
	
		player.setHealth(20);
		player.setSaturation(20);
		player.setFireTicks(0);
		//changement du skin
		ArrayList<Skins>  skins = Skins.listSkinDofus();
		MakeSkin makeSkin = new MakeSkin();
		int Randomnumber = new Random().nextInt(skins.size());
		makeSkin.loadSkin(player,skins.get(Randomnumber));

	}
	
	
	@EventHandler
	public void EntityDamageEvent(EntityDamageEvent  e) {
		GestionScoreboard gs = new GestionScoreboard();
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();
		
		if(paramPartie.isEncours()) {
			
		
			if(paramPartie.getModeDeJeux().getName().equalsIgnoreCase(this.getName())) {
				e.setCancelled(true);	
			}
		}
	}

	@EventHandler
	public void PlayerInteractEvent(PlayerInteractEntityEvent 	 e) {
		GestionScoreboard gs = new GestionScoreboard();
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();
		ListdesJoueur listJoueur =  paramPartie.getListJoueur();
		
		Player player = e.getPlayer();
		if(paramPartie.isEncours()) {

		
			if(paramPartie.getModeDeJeux().getName().equalsIgnoreCase(this.getName())) {
				
				if(e.getRightClicked() instanceof Sheep) {
					if(listJoueur.PlayerExist(player)) {
						Joueur joueur = listJoueur.getJoueur(player);
					
				
						if(player.getInventory().getItemInMainHand().getData().getItemType() == Material.LEGACY_SHEARS) {
							e.setCancelled(true);	
							Sheep sheep = (Sheep) e.getRightClicked();
							
					
							sheep.remove();
							ItemStack wool = new ItemStack(Material.getMaterial(sheep.getColor().name() + "_WOOL"), 1);
							player.getInventory().addItem(wool);
							
							joueur.setVie(0);
							joueur.setScore(CalculeScoreJoueur(player));
							
							listJoueur.suppJoueurtolist(joueur);
							listJoueur.addJoueurtolist(joueur);
							paramPartie.setListJoueur(listJoueur);
							bdd.exportParametrePartie(paramPartie);
							gs.MajScoreboardJeux(lancement);
						
						}
					}
				}
			}
		}
	}
	
	public Integer CalculeScoreJoueur(Player player) {
		Integer score =0;
		for(ItemStack item :player.getInventory().getContents()) {
			if(item!=null) {
	

				if(item.getType() == Material.WHITE_WOOL) {
					score =score+1*item.getAmount();
				}else if(item.getType() == Material.BLACK_WOOL) {
					score =score+2*item.getAmount();
				}else if(item.getType() == Material.PINK_WOOL){
					score =score+3*item.getAmount();
				}else if(item.getType() == Material.YELLOW_WOOL){
					score =score+4*item.getAmount();
				}
			}

		}
		return score;
		
	}
	public ArrayList<HashMap<String,Integer>> getArrayJoueurTrierByScore(){
		 HashMap<String,Integer> listPointsJoueur = new  HashMap<String,Integer>();
		
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();
		ListdesJoueur listJoueur =  paramPartie.getListJoueur();
		for(Joueur joueur : listJoueur.getlistJoueur()) {
			if(joueur.getMode()== Mode.JOUEUR) {
				listPointsJoueur.put(joueur.getNom(),joueur.getScore());
			}
		}
		return TrieHasMaptoArrayList(listPointsJoueur);
	}
	public ArrayList<HashMap<String,Integer>> TrieHasMaptoArrayList( HashMap<String,Integer> listPointsJoueur){
		ArrayList<HashMap<String,Integer>> listtrier = new ArrayList<HashMap<String,Integer>>();

			GestionScoreboard gs = new GestionScoreboard();
			basededonne bdd = new basededonne();
			ParametreDePartie paramPartie = bdd.importParametrePartie();
			ListdesJoueur listJoueur =  paramPartie.getListJoueur();

			
			 HashMap<Integer,ArrayList<String>> listPointsJoueurInverser = new HashMap<Integer,ArrayList<String>>();
			
			for(Entry<String, Integer> entry : listPointsJoueur.entrySet()){
				
				if(!(listPointsJoueurInverser.containsKey(entry.getValue()))) {
					ArrayList<String> listJo = new ArrayList<String>();
					
					listJo.add(entry.getKey());
					listPointsJoueurInverser.put(entry.getValue(), listJo);
					
					
				}else{
					ArrayList<String> listJo = listPointsJoueurInverser.get(entry.getValue());
					
					listJo.add(entry.getKey());
					
					listPointsJoueurInverser.put(entry.getValue(), listJo);
				}
			}
				TreeMap<Integer, ArrayList<String>> listPointsJoueurTrieTemp = new TreeMap<>(listPointsJoueurInverser);
				NavigableMap<Integer, ArrayList<String>> listPointsJoueurTrie = listPointsJoueurTrieTemp.descendingMap();
				
				
				for(Entry<Integer, ArrayList<String>> entry1 : listPointsJoueurTrie.entrySet()){
					 for(String str : entry1.getValue()) {
						 HashMap<String,Integer> tempmap = new  HashMap<String,Integer>();
						 tempmap.put(str, entry1.getKey());
						 listtrier.add(tempmap);
					 }	
				}		
	
			return listtrier;
		}
		
		
	
public void finDePartie(Joueur joueur) {
	GestionScoreboard gs = new GestionScoreboard();
	basededonne bdd = new basededonne();
	ParametreDePartie paramPartie = bdd.importParametrePartie();
	NoMode noMode = new NoMode(lancement);
	paramPartie.setModeDeJeux(noMode);
	paramPartie.setEncours(false);
	bdd.exportParametrePartie(paramPartie);

	Collection<? extends Player> listPlayerOnline =  Bukkit.getOnlinePlayers();
	
	for(Player player : listPlayerOnline) {

		
		 attenteduJeux attjeux = new attenteduJeux(lancement);
		 player.teleport(attjeux.getLoctionSpawn(player.getWorld()));
		 player.setGameMode(GameMode.ADVENTURE);
		 player.setFireTicks(0);
		 player.setHealth(20);
		 player.setSaturation(20);
		 player.getInventory().clear();
			ArrayList<Skins>  skins = Skins.listSkinConnection();
			MakeSkin makeSkin = new MakeSkin();
			int Randomnumber = new Random().nextInt(skins.size());
			makeSkin.loadSkin(player,skins.get(Randomnumber));
		 player.sendTitle("�4�lLe gagnant est :�9�l " + joueur.getNom(), "�b avec " + joueur.getScore()+" points. f�licitation", 40, 50, 20);
	}
	for(World world : Bukkit.getServer().getWorlds()){
        for(Entity entity : world.getEntities()){
            if(entity instanceof Sheep){
            	entity.remove();
            }
        }
	}
	gs.MajScoreboardJeux(lancement);
	
}

public void  spawnSheepAl�atoire() {
	
	new BukkitRunnable() {
        @Override
        public void run() {

        	timer = timer-1;
			
			basededonne bdd = new basededonne();
			ParametreDePartie paramPartie = bdd.importParametrePartie();
			ArenaParam arene = paramPartie.getArene();
			
			Location location = arene.aleaLocationSpawn();

			Sheep  sheep = location.getWorld().spawn(location,Sheep.class);
        	
			int Randomnumber = new Random().nextInt(20);
		
			switch((int)Randomnumber) {
			
				 
				case 0 :case 1 :case 2:case 3: case 4: case 5 :case 6:case 7: case 8 :case 9:
				{
					sheep.setColor(DyeColor.WHITE);
					break;
				}
				case 10: case 11: case 12:case 13: case 14: case 15: case 16:
				{
					sheep.setColor(DyeColor.BLACK);
					break;
				}
				case 17:case 18:
				{
					sheep.setColor(DyeColor.PINK);
					break;
				}
				case 19:
				{
					sheep.setColor(DyeColor.YELLOW);
					break;
				}
        	
        	}
		
	        if(timer==0) {
	
	    		ListdesJoueur listJoueur =  paramPartie.getListJoueur();
	    		ArrayList<HashMap<String,Integer>> listscorejoueurtrier = getArrayJoueurTrierByScore();
	    		Joueur joueurwin=null;

	    		for(Joueur joueur :listJoueur.getlistJoueur()) {
	    			
	    			if(joueur.getNom().equalsIgnoreCase(listscorejoueurtrier.get(0).entrySet().stream().findFirst().get().getKey())) {
	    				joueurwin=joueur;
	    			}
	    		}

	        	finDePartie(joueurwin);
	        	this.cancel();
	        }
        }

    	
	}.runTaskTimer(lancement, 0, 20);
}


}
