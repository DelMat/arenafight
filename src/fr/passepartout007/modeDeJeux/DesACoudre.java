package fr.passepartout007.modeDeJeux;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffectType;

import fr.passepartout007.gestionbasedonnee.ListdesJoueur;
import fr.passepartout007.gestionbasedonnee.basededonne;
import fr.passepartout007.gestionjoueur.Joueur;
import fr.passepartout007.gestionjoueur.Mode;
import fr.passepartout007.gestionjoueur.gestionSkin.MakeSkin;
import fr.passepartout007.gestionjoueur.gestionSkin.Skins;
import fr.passepartout007.gestionterrain.ArenaParam;
import fr.passepartout007.gestionterrain.creationNewTerrain;
import fr.passepartout007.lancementplugin.lancement;
import fr.passepartout007.phasedujeux.ParametreDePartie;
import fr.passepartout007.phasedujeux.attenteduJeux;
import fr.passepartout007.scoreboard.GestionScoreboard;
import fr.passepartout007.scoreboard.ScoreBoards;
import fr.passepartout007.scoreboard.ScoreboardCauchemar;
import fr.passepartout007.scoreboard.ScoreboardDesACoudre;

public class DesACoudre extends modeDejeux implements Serializable, Listener{
	
	public Location locSaut;

	public ScoreboardDesACoudre Scoarboard= new ScoreboardDesACoudre();
	public DesACoudre(lancement lancement) {
		super(lancement);
		super.setName("�c�lDesACoudre");
		super.setDescription( "�bChacun son tour en hauteur il vous faudra sauter, "
				+ "vis� un source d'eau a proximit� afin de survivre � cette chute. "
				+ "A chaque saut le bloc d'eau vis� sera remplacer par du sol, "
				+ "rester le dernier en vie. "
				+ "Bon courage.");
	}
	
	@Override
	public ScoreBoards getScorboard() {
		return this.Scoarboard;
	}
public void setLocationMap() {
	GestionScoreboard gs = new GestionScoreboard();
	basededonne bdd = new basededonne();
	
	ParametreDePartie paramPartie = bdd.importParametrePartie();
	ArenaParam arn = paramPartie.getArene();

	if(arn.getName().equalsIgnoreCase("Totoro")){
		Collection<? extends Player> listPlayerOnline =  Bukkit.getOnlinePlayers();
		Player player = null;
		for(Player players :listPlayerOnline) {
			player =players;
			break;
		}
		this.locSaut = new Location(player.getWorld(),-1257,59,-1146);
	}
			
}
public void lancementDePartie() {
	 	setLocationMap();
		GestionScoreboard gs = new GestionScoreboard();
		basededonne bdd = new basededonne();
		
		ParametreDePartie paramPartie = bdd.importParametrePartie();
	
		paramPartie.setEncours(true);
		bdd.exportParametrePartie(paramPartie);
		creationNewTerrain creationNewTerrain  =new creationNewTerrain();
		creationNewTerrain.copieTerrain(paramPartie.getArene().getName());
		
		Collection<? extends Player> listPlayerOnline =  Bukkit.getOnlinePlayers();
		ListdesJoueur listJoueur = paramPartie.getListJoueur();

		for(Player player :listPlayerOnline) {
			if(listJoueur.PlayerExist(player)) {
				Joueur joueur = listJoueur.getJoueur(player);
				if(joueur.getMode() == Mode.JOUEUR) {

			
					joueur.setVie(0);
					joueur.setScore(0);
					
					listJoueur.suppJoueurtolist(joueur);
					listJoueur.addJoueurtolist(joueur);
					paramPartie.setListJoueur(listJoueur);
					bdd.exportParametrePartie(paramPartie);
					teleporteJoueur(player);
					
					this.resetRoundJoueur(player);
					
					
					
				}else if(joueur.getMode() == Mode.SPECTATEUR) {

					teleporteSpectateur(player);

				}
			}else {
		
				teleporteSpectateur(player);

			}
		}
		int score = getMinScore();

		Joueur joueur2 =  getnextJoueur(score);

		Collection<? extends Player> listPlayerOnline1 =  Bukkit.getOnlinePlayers();
		for(Player play : listPlayerOnline1) {
			if(joueur2.getNom().equalsIgnoreCase(play.getName())) {
				
				play.teleport(locSaut);
				break;
			}
		}
		gs.MajScoreboardJeux(lancement);
	}
	public void teleporteSpectateur(Player player) {
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();

		player.teleport(paramPartie.getArene().aleaLocationSpawn());
		player.getWorld().setSpawnLocation(player.getLocation());
		player.setGameMode(GameMode.SPECTATOR);
		
	}
	
	public void teleporteJoueur(Player player) {
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();

		player.teleport(paramPartie.getArene().aleaLocationSpawn());
	
	
	}
	
	
	public void avantResetRoundJoueur(Player player) {

		player.getInventory().clear();
		player.setGameMode(GameMode.SPECTATOR);
	}
	public void resetRoundJoueur(Player player) {
		

		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();


		ScoreboardCauchemar scd = new ScoreboardCauchemar();

		

		player.setGameMode(GameMode.SURVIVAL);
		this.teleporteJoueur(player);
		player.getWorld().setSpawnLocation(player.getLocation());
		player.getInventory().clear();



		player.addPotionEffect(PotionEffectType.FIRE_RESISTANCE.createEffect(100, 3));
	
		player.setHealth(20);
		player.setSaturation(20);
		player.setFireTicks(0);
		
		//changement du skin
		ArrayList<Skins>  skins = Skins.listSkinTotoro();
		MakeSkin makeSkin = new MakeSkin();
		int Randomnumber = new Random().nextInt(skins.size());
		makeSkin.loadSkin(player,skins.get(Randomnumber));

	}
	
	
	@EventHandler
	public void EntityDamageEvent(EntityDamageEvent  e) {
	 	setLocationMap();
		GestionScoreboard gs = new GestionScoreboard();
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();
		ListdesJoueur listJoueur = paramPartie.getListJoueur();

		
		if(paramPartie.isEncours()) {
			
		
			if(paramPartie.getModeDeJeux().getName().equalsIgnoreCase(this.getName())) {
			
				
					if(e.getEntity() instanceof Player) {
							Player player = (Player) e.getEntity();
							
							double damage = e.getFinalDamage();
						if(player.getHealth() - damage <= 0) {
							e.setCancelled(true);
						if(e instanceof EntityDamageByEntityEvent){
					
							
					
							}else {
								
						
								if(listJoueur.PlayerExist(player)) {
									Joueur joueur1 = listJoueur.getJoueur(player);
									if(joueur1.getVie() !=0) {
										joueur1.setVie(joueur1.getVie()-1);
										listJoueur.suppJoueurtolist(joueur1);
										listJoueur.addJoueurtolist(joueur1);
										paramPartie.setListJoueur(listJoueur);
										bdd.exportParametrePartie(paramPartie);
		
										avantResetRoundJoueur(player);
						
							
										this.resetRoundJoueur(player);
			
										
			
										
									}else {
										joueur1.setMode(Mode.SPECTATEUR);
										listJoueur.suppJoueurtolist(joueur1);
										listJoueur.addJoueurtolist(joueur1);
										paramPartie.setListJoueur(listJoueur);
										bdd.exportParametrePartie(paramPartie);
										avantResetRoundJoueur(player);
										Integer nbJoueurLive =0;
										Joueur LastJoueur = null;
										for(Joueur jr : listJoueur.getlistJoueur()) {
											if(jr.getMode()==Mode.JOUEUR) {
												LastJoueur = jr;
												nbJoueurLive++;
											}
										}
										int score = getMinScore();
										Joueur joueur2 =  getnextJoueur(score);
								
										Collection<? extends Player> listPlayerOnline =  Bukkit.getOnlinePlayers();
										for(Player play : listPlayerOnline) {
											if(joueur2.getNom().equalsIgnoreCase(play.getName())) {
												play.teleport(locSaut);
												break;
											}
										}
										if(nbJoueurLive==1) {
											finDePartie(LastJoueur);
								

											gs.MajScoreboardJeux(lancement);
										}
										
									}
								}
							}
		

						gs.MajScoreboardJeux(lancement);
						}
					}
					
				}		
			}
		}
	
	
public void finDePartie(Joueur joueur) {
	basededonne bdd = new basededonne();
	ParametreDePartie paramPartie = bdd.importParametrePartie();
	ListdesJoueur listjouer = new ListdesJoueur();
	NoMode noMode = new NoMode(lancement);
	paramPartie.setModeDeJeux(noMode);
	paramPartie.setEncours(false);
	paramPartie.setListJoueur(listjouer);
	bdd.exportParametrePartie(paramPartie);

	Collection<? extends Player> listPlayerOnline =  Bukkit.getOnlinePlayers();
	for(Player player : listPlayerOnline) {

		
		 attenteduJeux attjeux = new attenteduJeux(lancement);
		 player.teleport(attjeux.getLoctionSpawn(player.getWorld()));
		 player.setGameMode(GameMode.ADVENTURE);
		 player.setFireTicks(0);
		 player.setHealth(20);
		 player.setSaturation(20);
		 player.getInventory().clear();
		 player.sendTitle("�4�lLe gagnant est :�9�l " + joueur.getNom(), "�b avec " + joueur.getScore()+" saut(s). f�licitation", 40, 50, 20);
	
			//changement du skin
			ArrayList<Skins>  skins = Skins.listSkinConnection();
			MakeSkin makeSkin = new MakeSkin();
			int Randomnumber = new Random().nextInt(skins.size());
			makeSkin.loadSkin(player,skins.get(Randomnumber));
	}
	
	
}

public int getMinScore() {
	basededonne bdd = new basededonne();
	ParametreDePartie paramPartie = bdd.importParametrePartie();


	ArrayList<Joueur> listejoueur= paramPartie.getListJoueur().getlistJoueur();
	int score = 6555;
	for(Joueur jr : listejoueur) {
		if(jr.getMode() == Mode.JOUEUR) {
			if( score >jr.getScore()) {
				score = jr.getScore();
				
			}
		}
	}
	return score;
}

public Joueur getnextJoueur(int score) {

		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();
		ArrayList<Joueur> newListJoueur = new ArrayList<Joueur>();

		ArrayList<Joueur> listejoueur= paramPartie.getListJoueur().getlistJoueur();
		Joueur joueur = null;
		for(Joueur jr : listejoueur) {
			if(jr.getMode() == Mode.JOUEUR) {
				if( jr.getScore() == score) {
	
					newListJoueur.add(jr);
					
				}
			}
		}
		int Randomnumber = new Random().nextInt(newListJoueur.size());
		Joueur Joueurplay  = newListJoueur.get(Randomnumber);
		
		return Joueurplay;
	
	
}
@EventHandler
public void onPlayerMove(PlayerMoveEvent e) {
 	
	GestionScoreboard gs = new GestionScoreboard();
	basededonne bdd = new basededonne();
	ParametreDePartie paramPartie = bdd.importParametrePartie();
	ListdesJoueur listJoueur = paramPartie.getListJoueur();

	
	if(paramPartie.isEncours()) {
		
	
		if(paramPartie.getModeDeJeux().getName().equalsIgnoreCase(this.getName())) {
			Player player = e.getPlayer();
			setLocationMap();
			Location location = player.getLocation();
			Material m = location.getBlock().getType();
			if (m == Material.LEGACY_STATIONARY_WATER || m == Material.WATER) {
				player.setGameMode(GameMode.SPECTATOR);
				if( listJoueur.PlayerExist(player)) {
					Joueur joueur = listJoueur.getJoueur(player);
				
					joueur.setScore(joueur.getScore()+1);
					
					
				
					listJoueur.suppJoueurtolist(joueur);
					listJoueur.addJoueurtolist(joueur);
					paramPartie.setListJoueur(listJoueur);
					bdd.exportParametrePartie(paramPartie);
		
					player.teleport(paramPartie.getArene().aleaLocationSpawn());
					player.setGameMode(GameMode.SURVIVAL);
					location.getBlock().setType(Material.GRASS_BLOCK);
					int score = getMinScore();
					Joueur joueur1 =  getnextJoueur(score);
			
					Collection<? extends Player> listPlayerOnline =  Bukkit.getOnlinePlayers();
					for(Player play : listPlayerOnline) {
						if(joueur1.getNom().equalsIgnoreCase(play.getName())) {
					
							play.teleport(locSaut);
							break;
						}
					}
				}
			
				
				
			}
					
		}
	}

}
}
