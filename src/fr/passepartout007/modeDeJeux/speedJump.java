package fr.passepartout007.modeDeJeux;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffectType;

import fr.passepartout007.gestionbasedonnee.JumpSpeedBDD;
import fr.passepartout007.gestionbasedonnee.ListdesJoueur;
import fr.passepartout007.gestionbasedonnee.basededonne;
import fr.passepartout007.gestionjoueur.Joueur;
import fr.passepartout007.gestionjoueur.Mode;
import fr.passepartout007.gestionterrain.Arena;
import fr.passepartout007.gestionterrain.creationNewTerrain;
import fr.passepartout007.lancementplugin.lancement;
import fr.passepartout007.phasedujeux.ParametreDePartie;
import fr.passepartout007.phasedujeux.attenteduJeux;
import fr.passepartout007.scoreboard.GestionScoreboard;
import fr.passepartout007.scoreboard.ScoreBoards;
import fr.passepartout007.scoreboard.ScoreboardCauchemar;
import fr.passepartout007.scoreboard.ScoreboardSpeedJump;

public class speedJump  extends modeDejeux implements Serializable, Listener{
	public ScoreboardSpeedJump Scoarboard= new ScoreboardSpeedJump();
	public HashMap<Integer,String> listbutton = new HashMap<Integer,String>();
	public speedJump(lancement lancement) {
		super(lancement);
		super.setName("�c�lSpeedJump");
		super.setDescription( "�bFaire le parcour de jump le plus vite possible, "
				+ "a chaque bouton cliquer dessus pour sauvegarder votre position. "
				+ "arriver sur les plaque de pression en premier et remporter la course, "
				+ "� chaque mort vous r�apparaissez a votre dernier point  de sauvegade. "
				+ "Sautez bien ;).");
	}
	
	@Override
	public ScoreBoards getScorboard() {
		return this.Scoarboard;
	}
	public void lancementDePartie() {
		GestionScoreboard gs = new GestionScoreboard();
		basededonne bdd = new basededonne();
		setAllButton();
		JumpSpeedBDD bddjumpsped = new JumpSpeedBDD();
		bddjumpsped.exportListButton(this.listbutton);;
		ParametreDePartie paramPartie = bdd.importParametrePartie();
	
		paramPartie.setEncours(true);
		bdd.exportParametrePartie(paramPartie);
		creationNewTerrain creationNewTerrain  =new creationNewTerrain();
		creationNewTerrain.copieTerrain(paramPartie.getArene().getName());
		
		Collection<? extends Player> listPlayerOnline =  Bukkit.getOnlinePlayers();
		ListdesJoueur listJoueur = paramPartie.getListJoueur();

		for(Player player :listPlayerOnline) {
			if(listJoueur.PlayerExist(player)) {
				Joueur joueur = listJoueur.getJoueur(player);
				if(joueur.getMode() == Mode.JOUEUR) {

			
					joueur.setVie(2);
					joueur.setScore(0);
					
					listJoueur.suppJoueurtolist(joueur);
					listJoueur.addJoueurtolist(joueur);
					paramPartie.setListJoueur(listJoueur);
					bdd.exportParametrePartie(paramPartie);
					teleporteJoueur(player);
					
					this.resetRoundJoueur(player);
					
					
					
				}else if(joueur.getMode() == Mode.SPECTATEUR) {
					joueur.setVie(0);
					joueur.setScore(0);
					
					listJoueur.suppJoueurtolist(joueur);
					listJoueur.addJoueurtolist(joueur);
					paramPartie.setListJoueur(listJoueur);
					bdd.exportParametrePartie(paramPartie);
					teleporteSpectateur(player);

				}
			}else {

				teleporteSpectateur(player);

			}
		}
		
		gs.MajScoreboardJeux(lancement);
		
		
		
		

	}
	

	public void resetRoundJoueur(Player player) {
		

		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();


		ScoreboardCauchemar scd = new ScoreboardCauchemar();

		

		player.setGameMode(GameMode.SURVIVAL);
		this.teleporteJoueur(player);

		player.getInventory().clear();


	
		player.addPotionEffect(PotionEffectType.FIRE_RESISTANCE.createEffect(100, 3));
	
		player.setHealth(20);
		player.setSaturation(20);
		player.setFireTicks(0);

	}
	
	public void teleporteSpectateur(Player player) {
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();
		ListdesJoueur listJoueur = paramPartie.getListJoueur();
		player.teleport(paramPartie.getArene().aleaLocationSpawn());
		Joueur joueur = paramPartie.getListJoueur().getJoueur(player);
		joueur.setlocationspawn(player.getLocation());

		listJoueur.suppJoueurtolist(joueur);
		listJoueur.addJoueurtolistSansImport(joueur);
		paramPartie.setListJoueur(listJoueur);
		bdd.exportParametrePartie(paramPartie);
		player.setGameMode(GameMode.SPECTATOR);
		
	}
	
	public void teleporteJoueur(Player player) {
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();
		ListdesJoueur listJoueur = paramPartie.getListJoueur();
		
		

		
		player.teleport(paramPartie.getArene().aleaLocationSpawn());
		Joueur joueur = paramPartie.getListJoueur().getJoueur(player);
		joueur.setlocationspawn(player.getLocation());

		listJoueur.suppJoueurtolist(joueur);
		listJoueur.addJoueurtolistSansImport(joueur);
		paramPartie.setListJoueur(listJoueur);
		bdd.exportParametrePartie(paramPartie);
	}


	
	@EventHandler
	public void onInteract(PlayerInteractEvent e){
		GestionScoreboard gs = new GestionScoreboard();
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();
		ListdesJoueur listJoueur = paramPartie.getListJoueur();
	
		
		if(paramPartie.isEncours()) {
			
		
			if(paramPartie.getModeDeJeux().getName().equalsIgnoreCase(this.getName())) {
		
				Player player = e.getPlayer();
				Action action = e.getAction();
				if( e.getClickedBlock()==null) {
					return;
				}
				Location location = e.getClickedBlock().getLocation();
		

				if ( location == null) {
					return;
				}
				World world = player.getWorld();
			
				if(world.getBlockAt(location) != null &&  action == Action.RIGHT_CLICK_BLOCK )
				{
					Block block = e.getClickedBlock();
			
			
					if (block.getType() == Material.JUNGLE_BUTTON) {
					
						Joueur joueur = listJoueur.getJoueur(player);
						joueur.setlocationspawn(player.getLocation());
						joueur.setScore(getnumButton(location));
	
						listJoueur.suppJoueurtolist(joueur);
						listJoueur.addJoueurtolistSansImport(joueur);
						paramPartie.setListJoueur(listJoueur);
						bdd.exportParametrePartie(paramPartie);
		
					}else if(block.getType() == Material.DARK_OAK_BUTTON){
						Joueur joueur = listJoueur.getJoueur(player);
						this.finDePartie(joueur);
					}
				}
				gs.MajScoreboardJeux(lancement);
			}
		}
		
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e) {
		GestionScoreboard gs = new GestionScoreboard();
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();

		
		if(paramPartie.isEncours()) {
			
		
			if(paramPartie.getModeDeJeux().getName().equalsIgnoreCase(this.getName())) {
				//si player game mode a faire
					Player player = e.getPlayer();
					Location location = player.getLocation();

					if(location.getBlockY()<0) {
						Joueur joueur = paramPartie.getListJoueur().getJoueur(player);
						player.teleport(joueur.getlocationspawn());
						player.setSaturation(20);
						
					}
			}
		}
	}
	
    
	@EventHandler
	public void EntityDamageEvent(EntityDamageEvent  e) {
		GestionScoreboard gs = new GestionScoreboard();
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();

	
		
		if(paramPartie.isEncours()) {
			
		
			if(paramPartie.getModeDeJeux().getName().equalsIgnoreCase(this.getName())) {
				e.setCancelled(true);
			}
		}
	}
	
	
	
	public void finDePartie(Joueur joueur) {
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();
		NoMode noMode = new NoMode(lancement);
		paramPartie.setModeDeJeux(noMode);
		paramPartie.setEncours(false);
		bdd.exportParametrePartie(paramPartie);

		Collection<? extends Player> listPlayerOnline =  Bukkit.getOnlinePlayers();
		for(Player player : listPlayerOnline) {
	
			 attenteduJeux attjeux = new attenteduJeux(lancement);

			 player.teleport(attjeux.getLoctionSpawn(player.getWorld()));
			 player.setGameMode(GameMode.ADVENTURE);
			 player.setFireTicks(0);
			 player.setHealth(20);
			 player.setSaturation(20);
			 player.getInventory().clear();
			 player.sendTitle("�4�lLe gagnant est :�9�l " + joueur.getNom()+"." ," f�licitation", 40, 50, 20);
		}
		
	}

	public void setAllButton() {
		Arena arn = new Arena();

		
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();
		String name = paramPartie.getArene().getName();
		String chemin = arn.basededonne.CheminFichier(name + ".txt");
		HashMap<Double,String> buttonlocationZ = new HashMap<Double,String>();;
		HashMap<String,String> blockmap = arn.importArenaData(chemin);
		int nbButton = 0;
		int finalButton=0;
		for (Entry<String, String> data : blockmap.entrySet()) {		
			if(arn.inArena(arn.stringToLocation(data.getKey()))) {
				if(Bukkit.createBlockData(data.getValue()).getMaterial() == Material.JUNGLE_BUTTON) {
					nbButton++;
			
					buttonlocationZ.put(arn.stringToLocation(data.getKey()).getZ(),data.getKey());
		
					
					
				}
				
			}
		}
		
		
		  Map sortedMap = new TreeMap(buttonlocationZ);
		  
		  Set set2 = sortedMap.entrySet();
	      Iterator iterator2 = set2.iterator();
	
	  
	      int nbbutton=0;
	      while(iterator2.hasNext()) {
	    	  nbbutton++;
	         Map.Entry me2 = (Map.Entry)iterator2.next();
	         listbutton.put(nbbutton, (String) me2.getValue());
	         
	      }
	}

	public int getnumButton(Location location) {
		Arena arn = new Arena();
		JumpSpeedBDD bddjumpsped = new JumpSpeedBDD();
		listbutton =bddjumpsped.importListButton();
		int numButton =0;

		for(Entry<Integer, String> data : listbutton.entrySet()) {
			if(data.getValue().equalsIgnoreCase(arn.locattostring(location))) {
				numButton =data.getKey();
			}
		}

		return numButton;
	}

}


