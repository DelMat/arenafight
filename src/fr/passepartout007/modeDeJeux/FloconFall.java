package fr.passepartout007.modeDeJeux;

import java.io.Serializable;
import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;

import fr.passepartout007.gestionbasedonnee.ListdesJoueur;
import fr.passepartout007.gestionbasedonnee.basededonne;
import fr.passepartout007.gestionjoueur.Joueur;
import fr.passepartout007.gestionjoueur.Mode;
import fr.passepartout007.gestionterrain.creationNewTerrain;
import fr.passepartout007.lancementplugin.lancement;
import fr.passepartout007.phasedujeux.ParametreDePartie;
import fr.passepartout007.phasedujeux.attenteduJeux;
import fr.passepartout007.scoreboard.GestionScoreboard;
import fr.passepartout007.scoreboard.ScoreBoards;
import fr.passepartout007.scoreboard.ScoreboardAttenteDuJeux;
import fr.passepartout007.scoreboard.ScoreboardDuel;
import fr.passepartout007.scoreboard.ScoreboardFloconFall;

public class FloconFall extends modeDejeux implements Serializable, Listener{
	
	public ScoreBoards Scoarboard= new ScoreboardFloconFall();
	
	
	public FloconFall(lancement lancement) {
		super(lancement);
		super.setName("�c�lFloconFall");
		super.setDescription( "�bIl est question de tuer ses adversaires, "
				+ "mais d'une facon bien particuli�re. "
				+ "�quip� d'une pelle creus� sur les pieds de tes adversaires, "
				+ "une fois tout en bas il mourera dans la lave. "
				+ "bonne chance ;)");
	}
	
	
	public ScoreBoards getScorboard() {
	return this.Scoarboard;
		
	}

	
	public void lancementDePartie() {
		GestionScoreboard gs = new GestionScoreboard();
	
		basededonne bdd = new basededonne();

		ParametreDePartie paramPartie = bdd.importParametrePartie();

		paramPartie.setEncours(true);
		bdd.exportParametrePartie(paramPartie);
		creationNewTerrain creationNewTerrain  =new creationNewTerrain();
		creationNewTerrain.copieTerrain(paramPartie.getArene().getName());
		
		Collection<? extends Player> listPlayerOnline =  Bukkit.getOnlinePlayers();
		ListdesJoueur listJoueur = paramPartie.getListJoueur();
	
		for(Player player :listPlayerOnline) {
			if(listJoueur.PlayerExist(player)) {
				Joueur joueur = listJoueur.getJoueur(player);
				if(joueur.getMode() == Mode.JOUEUR) {

			
					joueur.setVie(0);
					joueur.setScore(0);
					
					listJoueur.suppJoueurtolist(joueur);
					listJoueur.addJoueurtolist(joueur);
					paramPartie.setListJoueur(listJoueur);
					bdd.exportParametrePartie(paramPartie);
					teleporteJoueur(player);
					

					
					
					
				}else if(joueur.getMode() == Mode.SPECTATEUR) {
			
					teleporteSpectateur(player);
					ScoreboardDuel scd = new ScoreboardDuel();
					//scd.ScoreboardDuelSpecateur(player);
				}
				
			
				
			}else {
			
				teleporteSpectateur(player);

			}
		
		}
		gs.MajScoreboardJeux(lancement);
		
	}
	
	
	public void teleporteSpectateur(Player player) {
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();

		player.teleport(paramPartie.getArene().aleaLocationSpawn());
		player.getWorld().setSpawnLocation(player.getLocation());
		player.setGameMode(GameMode.SPECTATOR);
		
	}
	
	public void teleporteJoueur(Player player) {
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();

		player.teleport(paramPartie.getArene().aleaLocationSpawn());
		player.setGameMode(GameMode.SURVIVAL);
		player.getInventory().addItem(new ItemStack(Material.DIAMOND_SHOVEL));
	}


	@EventHandler
	public void EntityDamageEvent(EntityDamageEvent  e) {
		GestionScoreboard gs = new GestionScoreboard();
		
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();
		ListdesJoueur listJoueur = paramPartie.getListJoueur();
		Collection<? extends Player> listPlayerOnline =  Bukkit.getOnlinePlayers();
	

		if(paramPartie.isEncours()) {

		
			if(paramPartie.getModeDeJeux().getName().equalsIgnoreCase(this.getName())) {
		
					if(e.getEntity() instanceof Player) {
							Player player = (Player) e.getEntity();
		
							double damage = e.getFinalDamage();
						if(player.getHealth() - damage <= 0) {
	
							e.setCancelled(true);
							Player lastPlayer=null;
							Integer nbJoueurResant =0;
							player.setGameMode(GameMode.SPECTATOR);
							
							for(Player onlineplayer : listPlayerOnline) {
								if(onlineplayer.getGameMode()==GameMode.SURVIVAL) {
									
						
									nbJoueurResant++;
									lastPlayer = onlineplayer;
								}
								
							}
					
							if(nbJoueurResant==1) {
								finDePartie(listJoueur.getJoueur(lastPlayer));
							}
						}
					}
			}
		}
		gs.MajScoreboardJeux(lancement);
	}
	
	@EventHandler
	public void EntityDamageEvent(EntityDamageByEntityEvent e) {
		GestionScoreboard gs = new GestionScoreboard();
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();

	

		if(paramPartie.isEncours()) {

		
			if(paramPartie.getModeDeJeux().getName().equalsIgnoreCase(this.getName())) {
				e.setCancelled(true);	
			}
		}
		gs.MajScoreboardJeux(lancement);
	}
	
	public void finDePartie(Joueur joueur) {
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();
		NoMode noMode = new NoMode(lancement);
		paramPartie.setModeDeJeux(noMode);
		paramPartie.setEncours(false);
		bdd.exportParametrePartie(paramPartie);
	
		Collection<? extends Player> listPlayerOnline =  Bukkit.getOnlinePlayers();
		for(Player player : listPlayerOnline) {
			ScoreboardAttenteDuJeux boardADJ = new ScoreboardAttenteDuJeux();
			boardADJ.ScoreboardJoueur(player, "", lancement);
			
			 attenteduJeux attjeux = new attenteduJeux(lancement);
			 player.teleport(attjeux.getLoctionSpawn(player.getWorld()));
			 player.getWorld().setSpawnLocation(player.getLocation());
			 player.setGameMode(GameMode.ADVENTURE);
			 player.setFireTicks(0);
			 player.setHealth(20);
			 player.setSaturation(20);
			 player.getInventory().clear();
			 player.setFireTicks(0);
			 player.sendTitle("�4�lLe gagnant est :�9�l " + joueur.getNom(), "�bF�licitation", 40, 50, 20);
		}
		
	}


}