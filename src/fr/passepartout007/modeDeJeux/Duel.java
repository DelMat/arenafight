package fr.passepartout007.modeDeJeux;

import java.io.Serializable;
import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.potion.PotionEffectType;

import fr.passepartout007.gestionbasedonnee.ListdesJoueur;
import fr.passepartout007.gestionbasedonnee.basededonne;
import fr.passepartout007.gestionjoueur.EquipementJoueur2;
import fr.passepartout007.gestionjoueur.Joueur;
import fr.passepartout007.gestionjoueur.Mode;
import fr.passepartout007.gestionterrain.creationNewTerrain;
import fr.passepartout007.lancementplugin.lancement;
import fr.passepartout007.phasedujeux.ParametreDePartie;
import fr.passepartout007.phasedujeux.attenteduJeux;
import fr.passepartout007.scoreboard.GestionScoreboard;
import fr.passepartout007.scoreboard.ScoreBoards;
import fr.passepartout007.scoreboard.ScoreboardDuel;

public class Duel extends modeDejeux implements Serializable, Listener{
	
	public ScoreboardDuel Scoarboard= new ScoreboardDuel();
	


	
	public Duel(lancement lancement) {
		super(lancement);
		super.setName("�c�lDuel");
		super.setDescription( "�b"
				+ "Ce mode de jeux est comme sont nom "
				+ "l'indique un duel entre deux joueur."
				+ " Vous �tes dans une ar�ne et la r�gle est simple, battre son adversaire."
				+ " A chaque fois que ton adversaire est tu�"
				+ " la manche recommence, pas le temps de se reposer"
				+ " le premier � 5 points remporte la partie.");
	}
	
	public ScoreBoards getScorboard() {
	return this.Scoarboard;
		
	}
	
	public void lancementDePartie() {
		
		basededonne bdd = new basededonne();
		GestionScoreboard gs = new GestionScoreboard();
		ParametreDePartie paramPartie = bdd.importParametrePartie();
	
		paramPartie.setEncours(true);
		bdd.exportParametrePartie(paramPartie);
		creationNewTerrain creationNewTerrain  =new creationNewTerrain();
		creationNewTerrain.copieTerrain(paramPartie.getArene().getName());
		
		Collection<? extends Player> listPlayerOnline =  Bukkit.getOnlinePlayers();
		ListdesJoueur listJoueur = paramPartie.getListJoueur();
	
		for(Player player :listPlayerOnline) {
			if(listJoueur.PlayerExist(player)) {
				Joueur joueur = listJoueur.getJoueur(player);
				if(joueur.getMode() == Mode.JOUEUR) {

	
					joueur.setVie(5);
					joueur.setScore(0);
					
					listJoueur.suppJoueurtolist(joueur);
					listJoueur.addJoueurtolist(joueur);
					paramPartie.setListJoueur(listJoueur);
					bdd.exportParametrePartie(paramPartie);
					teleporteJoueur(player);
					
					this.resetRoundJoueur(player);
					



					
				}else if(joueur.getMode() == Mode.SPECTATEUR) {
					
					
					teleporteSpectateur(player);
		
				}
			}else {
				
				teleporteSpectateur(player);
		

			}
		}
		gs.MajScoreboardJeux(lancement);

	}
	public void teleporteSpectateur(Player player) {
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();

		player.teleport(paramPartie.getArene().aleaLocationSpawn());
		player.getWorld().setSpawnLocation(player.getLocation());
		player.setGameMode(GameMode.SPECTATOR);
		
	}
	
	public void teleporteJoueur(Player player) {
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();

		player.teleport(paramPartie.getArene().aleaLocationSpawn());
	
	}
	
	public void avantResetRoundJoueur(Player player) {

		player.getInventory().clear();
		player.setGameMode(GameMode.SPECTATOR);
	}
	public void resetRoundJoueur(Player player) {
	

		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();


		

		player.setGameMode(GameMode.SURVIVAL);
		this.teleporteJoueur(player);
		player.getWorld().setSpawnLocation(player.getLocation());
		player.getInventory().clear();

		EquipementJoueur2 equip = new EquipementJoueur2();
		equip.ImportEquipement();
		equip.equipeJoueur(player);
		player.addPotionEffect(PotionEffectType.FIRE_RESISTANCE.createEffect(100, 3));
		player.setFireTicks(0);
		player.setHealth(20);
		player.setSaturation(20);
		player.setFireTicks(0);
		player.setFireTicks(0);
		player.setFireTicks(0);
	}
	
	
	@EventHandler
	public void EntityDamageEvent(EntityDamageEvent  e) {
		
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();
		ListdesJoueur listJoueur = paramPartie.getListJoueur();
		
		Player playeralive=null;
		
		if(paramPartie.isEncours()) {
			
		
			if(paramPartie.getModeDeJeux().getName().equalsIgnoreCase(this.getName())) {
			
				
					if(e.getEntity() instanceof Player) {
							Player player = (Player) e.getEntity();
							
							double damage = e.getFinalDamage();
						if(player.getHealth() - damage <= 0) {
							e.setCancelled(true);
	
							for(Joueur joueur : listJoueur.getlistJoueursansImport()) {
	
								if(joueur.getMode() == Mode.JOUEUR && !(joueur.getNom().equalsIgnoreCase(player.getName()))) {
									for( Player player1 :Bukkit.getOnlinePlayers()) {
										if(player1.getName().equalsIgnoreCase(joueur.getNom())) {
										playeralive = player1;
									
										}
									}
								}
							}				
				
							if(listJoueur.PlayerExist(player)) {
								Joueur joueur1 = listJoueur.getJoueur(player);
								if(joueur1.getVie() !=0) {
									joueur1.setVie(joueur1.getVie()-1);
									listJoueur.suppJoueurtolist(joueur1);
									listJoueur.addJoueurtolist(joueur1);
									paramPartie.setListJoueur(listJoueur);
									creationNewTerrain creationNewTerrain = new creationNewTerrain();
	
									avantResetRoundJoueur(player);
									avantResetRoundJoueur(playeralive);
									creationNewTerrain.copieTerrain(paramPartie.getArene().getName());
									this.resetRoundJoueur(player);
									this.resetRoundJoueur(playeralive);
									GestionScoreboard gs = new GestionScoreboard();

									gs.MajScoreboardJeux(lancement);
		
									
								}else {
									this.finDePartie(listJoueur.getJoueur(playeralive));
									GestionScoreboard gs = new GestionScoreboard();

									gs.MajScoreboardJeux(lancement);
								}
							}
							
						}
					}
			}
		}	
}

	@EventHandler
	public void EntityDamageEvent(EntityDamageByEntityEvent e) {
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();

		if(paramPartie.isEncours()) {
			if(paramPartie.getModeDeJeux().getName().equalsIgnoreCase(this.getName())) {
				
			
			
				if(e.getEntity() instanceof Player) {
					double damage = e.getFinalDamage();
					
	
					ListdesJoueur listJoueur = paramPartie.getListJoueur();
		
					Player playeralive=null;
		
					Player player = (Player) e.getEntity();
		
					if(player.getHealth() - damage <= 0) {
						e.setCancelled(true);
				
						
	
			
						
						if(paramPartie.getModeDeJeux().getName().equalsIgnoreCase(super.getName())){
							 Entity damager =   e.getDamager();
				
						
							if(damager instanceof Player){
								Player killer = (Player) damager;
				
								
								if(listJoueur.PlayerExist(killer) && listJoueur.PlayerExist(player)){
									Joueur joueurkiller = listJoueur.getJoueur(killer);
									Joueur joueur = listJoueur.getJoueur(player);
									if(joueurkiller.getScore() != 4 && joueur.getVie() !=0){
										
										joueur.setVie(joueur.getVie()-1);
										joueurkiller.setScore(joueurkiller.getScore()+1);
										listJoueur.suppJoueurtolist(joueurkiller);
										listJoueur.suppJoueurtolist(joueur);
										listJoueur.addJoueurtolist(joueur);
										listJoueur.addJoueurtolist(joueurkiller);
										paramPartie.setListJoueur(listJoueur);
										bdd.exportParametrePartie(paramPartie);
										
										
			
										EquipementJoueur2 equip = new EquipementJoueur2();
										equip.CreerNewEquipement();
										creationNewTerrain creationNewTerrain = new creationNewTerrain();
										
										avantResetRoundJoueur(player);
										avantResetRoundJoueur(killer);
										creationNewTerrain.copieTerrain(paramPartie.getArene().getName());
										this.resetRoundJoueur(player);
										this.resetRoundJoueur(killer);
										GestionScoreboard gs = new GestionScoreboard();

										gs.MajScoreboardJeux(lancement);
										
									}else{
										this.finDePartie(joueurkiller);
										GestionScoreboard gs = new GestionScoreboard();

										gs.MajScoreboardJeux(lancement);
									};
								}
					
							}else {
								if(listJoueur.PlayerExist(player)) {
									Joueur joueur = listJoueur.getJoueur(player);
									if(joueur.getVie() !=0) {
										joueur.setVie(joueur.getVie()-1);
										listJoueur.suppJoueurtolist(joueur);
										listJoueur.addJoueurtolist(joueur);
										paramPartie.setListJoueur(listJoueur);
										creationNewTerrain creationNewTerrain = new creationNewTerrain();
										
										avantResetRoundJoueur(player);
										avantResetRoundJoueur(playeralive);
										creationNewTerrain.copieTerrain(paramPartie.getArene().getName());
										this.resetRoundJoueur(player);
										this.resetRoundJoueur(playeralive);
										
										GestionScoreboard gs = new GestionScoreboard();

										gs.MajScoreboardJeux(lancement);
										
									}else {
										this.finDePartie(listJoueur.getJoueur(playeralive));
										GestionScoreboard gs = new GestionScoreboard();

										gs.MajScoreboardJeux(lancement);
									}
								}
							}
						}
					}
				}
			}
		}
	}
		
	public void finDePartie(Joueur joueur) {
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();
		NoMode noMode = new NoMode(lancement);
		paramPartie.setModeDeJeux(noMode);
		paramPartie.setEncours(false);
		bdd.exportParametrePartie(paramPartie);
	
		Collection<? extends Player> listPlayerOnline =  Bukkit.getOnlinePlayers();
		for(Player player : listPlayerOnline) {
	

			 
			
			 attenteduJeux attjeux = new attenteduJeux(lancement);
			 player.teleport(attjeux.getLoctionSpawn(player.getWorld()));
			 player.setGameMode(GameMode.ADVENTURE);
			 player.setFireTicks(0);
			 player.setHealth(20);
			 player.setSaturation(20);
			 player.getInventory().clear();
			 player.sendTitle("�4�lLe gagnant est :�9�l " + joueur.getNom(), "�bF�licitation", 40, 50, 20);
		}
		
	}



	
}
