package fr.passepartout007.modeDeJeux;

import java.io.Serializable;
import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import fr.passepartout007.gestionterrain.Arena;
import fr.passepartout007.gestionterrain.ArenaParam;
import fr.passepartout007.lancementplugin.lancement;
import fr.passepartout007.scoreboard.ScoreBoards;


public abstract class modeDejeux implements Serializable{
	public transient lancement lancement;
	private String description;
	private String name;
	private ArrayList<modeDejeux> listModedeJeux = new ArrayList<modeDejeux>();
	

	public modeDejeux(lancement lancement){
		this.lancement=lancement;
	}
	public abstract ScoreBoards getScorboard();


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void chatDesciption(Player player) {

		player.sendMessage(this.getName());
		player.sendMessage(this.description);
	}
	
	public  void setListModedeJeux(){
		Duel duel = new Duel(lancement);
		Cauchemar cauchemar = new Cauchemar(lancement);
		FloconFall floconFall = new FloconFall(lancement);
		NoMode noMode = new NoMode(lancement);
		TonteBouftou tonteBouftou = new TonteBouftou(lancement);
		DesACoudre desACoudre = new DesACoudre(lancement);
		speedJump speedJump = new speedJump(lancement);
		this.listModedeJeux.clear();
		
		this.listModedeJeux.add(speedJump);
		this.listModedeJeux.add(desACoudre);
		this.listModedeJeux.add(cauchemar);
		this.listModedeJeux.add(noMode);
		this.listModedeJeux.add(floconFall);
		this.listModedeJeux.add(duel);
		this.listModedeJeux.add(tonteBouftou);
		
		

	}
		
	
	public  ArrayList<modeDejeux> getListModedeJeux(){
		this.setListModedeJeux();
		return this.listModedeJeux;
		
	}
	
	public modeDejeux getModedejeux(String string) {
		modeDejeux mdj =null;
		ArrayList<modeDejeux> listMDJ =getListModedeJeux();
		for(modeDejeux MDJ : listMDJ) {
			if(MDJ.getName().equalsIgnoreCase("�c�l"+string)) {
				return MDJ;
	
			}
		}
		
		return mdj;
	}
	
	public modeDejeux getModedeJeux(String str) {
		ArrayList<modeDejeux> listmdj = this.getListModedeJeux();
		boolean traiter = false;
		for(modeDejeux mdj : listmdj) {
			if(mdj.getName().equalsIgnoreCase(str)){
				return mdj;
			}
		}
		return null;
	}
	public String getnextmodeDeJeux(Player player){
		String modeDeJeux = null;
		World world = player.getWorld();
		Arena arena = new Arena();
		ArrayList<ArenaParam> listarenaParameter = arena.importArenaParameterList();
		Location locModeJeuxArena = new Location(player.getWorld(),-1290,49,-1176);
		Location locNomArena = new Location(player.getWorld(),-1290,49,-1172);
		if(world.getBlockAt(locModeJeuxArena) != null && world.getBlockAt(locNomArena) != null ){
			BlockState bslMJA = world.getBlockAt(locModeJeuxArena).getState();
			BlockState bsNA = world.getBlockAt(locNomArena).getState();
			
			if (bslMJA instanceof Sign && bsNA instanceof Sign){
				Sign signNomArena = (Sign) bslMJA;
				Sign signModeJeuxArena = (Sign) bsNA;
				
				
				if(signModeJeuxArena.getLine(0).equalsIgnoreCase("�e�lMode de jeux") && signNomArena.getLine(0).equalsIgnoreCase("�e�lArena")){
					boolean traiter = false;
					for(ArenaParam arnpram : listarenaParameter) {
						
						if(("�c�l" + arnpram.getName()).equalsIgnoreCase(signNomArena.getLine(2))) {
						
							ArrayList<modeDejeux> Listmdj =arnpram.getModeDejeux();
						
							for(modeDejeux mdj :Listmdj) {
								
								System.out.println(signModeJeuxArena.getLine(2));
								System.out.println(mdj.getName());
								if(mdj.getName().equalsIgnoreCase(signModeJeuxArena.getLine(2))) {
									
						
									if (Listmdj.indexOf(mdj) +1 < Listmdj.size()) {
							
										modeDeJeux = Listmdj.get(Listmdj.indexOf(mdj)+1).getName();
										traiter = true;
									}

								}
					
								if(traiter==false) {
									modeDeJeux = Listmdj.get(0).getName();
								}
							}
						
							
						}
					}
					
				}
				
			}
		}

		return modeDeJeux;

		
	}


	
}
