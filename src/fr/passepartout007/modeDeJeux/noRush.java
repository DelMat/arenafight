package fr.passepartout007.modeDeJeux;

import java.io.Serializable;
import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.potion.PotionEffectType;

import fr.passepartout007.gestionbasedonnee.ListdesJoueur;
import fr.passepartout007.gestionbasedonnee.basededonne;
import fr.passepartout007.gestionjoueur.EquipementJoueur2;
import fr.passepartout007.gestionjoueur.Joueur;
import fr.passepartout007.gestionjoueur.Mode;
import fr.passepartout007.gestionterrain.creationNewTerrain;
import fr.passepartout007.lancementplugin.lancement;
import fr.passepartout007.phasedujeux.ParametreDePartie;
import fr.passepartout007.phasedujeux.attenteduJeux;
import fr.passepartout007.scoreboard.GestionScoreboard;
import fr.passepartout007.scoreboard.ScoreBoards;
import fr.passepartout007.scoreboard.ScoreboardAttenteDuJeux;
import fr.passepartout007.scoreboard.ScoreboardCauchemar;

public class noRush extends modeDejeux implements Serializable, Listener{
	public ScoreboardCauchemar Scoarboard= new ScoreboardCauchemar();
	public noRush(lancement lancement) {
		super(lancement);
		super.setName("�c�lnoRush");
		super.setDescription( "�bEn �quipe al�atoire, "
				+ "tuer la team adverse. "
				+ "chaqu'un de son cot� de la map, "
				+ "il faudras bien viser."
				+ "Pour gagner la derni�re �quipe en vie.");
	}
	
	@Override
	public ScoreBoards getScorboard() {
		return this.Scoarboard;
	}
	
public void lancementDePartie() {
		GestionScoreboard gs = new GestionScoreboard();
		basededonne bdd = new basededonne();
		
		ParametreDePartie paramPartie = bdd.importParametrePartie();
	
		paramPartie.setEncours(true);
		bdd.exportParametrePartie(paramPartie);
		creationNewTerrain creationNewTerrain  =new creationNewTerrain();
		creationNewTerrain.copieTerrain(paramPartie.getArene().getName());
		
		Collection<? extends Player> listPlayerOnline =  Bukkit.getOnlinePlayers();
		ListdesJoueur listJoueur = paramPartie.getListJoueur();

		for(Player player :listPlayerOnline) {
			if(listJoueur.PlayerExist(player)) {
				Joueur joueur = listJoueur.getJoueur(player);
				if(joueur.getMode() == Mode.JOUEUR) {

			
					joueur.setVie(2);
					joueur.setScore(0);
					
					listJoueur.suppJoueurtolist(joueur);
					listJoueur.addJoueurtolist(joueur);
					paramPartie.setListJoueur(listJoueur);
					bdd.exportParametrePartie(paramPartie);
					teleporteJoueur(player);
					
					this.resetRoundJoueur(player);
					
					
					
				}else if(joueur.getMode() == Mode.SPECTATEUR) {

					teleporteSpectateur(player);

				}
			}else {
		
				teleporteSpectateur(player);

			}
		}
		
		gs.MajScoreboardJeux(lancement);
	}
	public void teleporteSpectateur(Player player) {
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();

		player.teleport(paramPartie.getArene().aleaLocationSpawn());
		player.getWorld().setSpawnLocation(player.getLocation());
		player.setGameMode(GameMode.SPECTATOR);
		
	}
	
	public void teleporteJoueur(Player player) {
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();

		player.teleport(paramPartie.getArene().aleaLocationSpawn());
	
	
	}
	
	
	public void avantResetRoundJoueur(Player player) {

		player.getInventory().clear();
		player.setGameMode(GameMode.SPECTATOR);
	}
	public void resetRoundJoueur(Player player) {
		

		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();


		ScoreboardCauchemar scd = new ScoreboardCauchemar();

		

		player.setGameMode(GameMode.SURVIVAL);
		this.teleporteJoueur(player);
		player.getWorld().setSpawnLocation(player.getLocation());
		player.getInventory().clear();


		EquipementJoueur2 equip = new EquipementJoueur2();
		equip.ImportEquipement();
		equip.equipeJoueur(player);
		player.addPotionEffect(PotionEffectType.FIRE_RESISTANCE.createEffect(100, 3));
	
		player.setHealth(20);
		player.setSaturation(20);
		player.setFireTicks(0);

	}
	
	
	@EventHandler
	public void EntityDamageEvent(EntityDamageEvent  e) {
		GestionScoreboard gs = new GestionScoreboard();
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();
		ListdesJoueur listJoueur = paramPartie.getListJoueur();
		System.out.println("on passe la ");
		
		if(paramPartie.isEncours()) {
			
		
			if(paramPartie.getModeDeJeux().getName().equalsIgnoreCase(this.getName())) {
			
				
					if(e.getEntity() instanceof Player) {
							Player player = (Player) e.getEntity();
							
							double damage = e.getFinalDamage();
						if(player.getHealth() - damage <= 0) {
							e.setCancelled(true);
						if(e instanceof EntityDamageByEntityEvent){
								EntityDamageByEntityEvent edbeEvent = (EntityDamageByEntityEvent)e;
							
								Entity damager =   edbeEvent.getDamager();
								
								
								if(damager instanceof Player){
									Player killer = (Player) damager;
									System.out.println("ON ARRIVE LA1");
									
									if(listJoueur.PlayerExist(killer) && listJoueur.PlayerExist(player)){
										System.out.println("ON ARRIVE LA2");
										Joueur joueurkiller = listJoueur.getJoueur(killer);
										System.out.println("le tueur est : " + joueurkiller.getNom());
										Joueur joueur1 = listJoueur.getJoueur(player);
										if(joueur1.getVie() !=0){
											System.out.println("kill sur joueur");
											joueur1.setVie(joueur1.getVie()-1);
											joueurkiller.setScore(joueurkiller.getScore()+1);
											listJoueur.suppJoueurtolist(joueurkiller);
											listJoueur.suppJoueurtolist(joueur1);
											listJoueur.addJoueurtolist(joueur1);
											listJoueur.addJoueurtolist(joueurkiller);
											paramPartie.setListJoueur(listJoueur);
											bdd.exportParametrePartie(paramPartie);
											
											avantResetRoundJoueur(player);
	
	
											this.resetRoundJoueur(player);
										
											
										}else {
											joueur1.setMode(Mode.SPECTATEUR);
											listJoueur.suppJoueurtolist(joueur1);
											listJoueur.addJoueurtolist(joueur1);
											joueurkiller.setScore(joueurkiller.getScore()+1);
											listJoueur.suppJoueurtolist(joueurkiller);
											listJoueur.addJoueurtolist(joueurkiller);
											paramPartie.setListJoueur(listJoueur);
											bdd.exportParametrePartie(paramPartie);
											avantResetRoundJoueur(player);
											Integer nbJoueurLive =0;
											Joueur LastJoueur = null;
											for(Joueur jr : listJoueur.getlistJoueur()) {
												if(jr.getMode()==Mode.JOUEUR) {
													LastJoueur = jr;
													nbJoueurLive++;
												}
											}
											if(nbJoueurLive==1) {
												finDePartie(LastJoueur);
											}
										}
									}
								}
							}else {
								
						
								if(listJoueur.PlayerExist(player)) {
									Joueur joueur1 = listJoueur.getJoueur(player);
									if(joueur1.getVie() !=0) {
										joueur1.setVie(joueur1.getVie()-1);
										listJoueur.suppJoueurtolist(joueur1);
										listJoueur.addJoueurtolist(joueur1);
										paramPartie.setListJoueur(listJoueur);
										bdd.exportParametrePartie(paramPartie);
		
										avantResetRoundJoueur(player);
						
							
										this.resetRoundJoueur(player);
			
										
			
										
									}else {
										joueur1.setMode(Mode.SPECTATEUR);
										listJoueur.suppJoueurtolist(joueur1);
										listJoueur.addJoueurtolist(joueur1);
										paramPartie.setListJoueur(listJoueur);
										bdd.exportParametrePartie(paramPartie);
										avantResetRoundJoueur(player);
										Integer nbJoueurLive =0;
										Joueur LastJoueur = null;
										for(Joueur jr : listJoueur.getlistJoueur()) {
											if(jr.getMode()==Mode.JOUEUR) {
												LastJoueur = jr;
												nbJoueurLive++;
											}
										}
										if(nbJoueurLive==1) {
											finDePartie(LastJoueur);
								

											gs.MajScoreboardJeux(lancement);
										}
									}
								}
							}
		

						gs.MajScoreboardJeux(lancement);
						}
					}
					
				}		
			}
		}
	
	
public void finDePartie(Joueur joueur) {
	basededonne bdd = new basededonne();
	ParametreDePartie paramPartie = bdd.importParametrePartie();
	NoMode noMode = new NoMode(lancement);
	paramPartie.setModeDeJeux(noMode);
	paramPartie.setEncours(false);
	bdd.exportParametrePartie(paramPartie);

	Collection<? extends Player> listPlayerOnline =  Bukkit.getOnlinePlayers();
	for(Player player : listPlayerOnline) {

		
		 attenteduJeux attjeux = new attenteduJeux(lancement);
		 player.teleport(attjeux.getLoctionSpawn(player.getWorld()));
		 player.setGameMode(GameMode.ADVENTURE);
		 player.setFireTicks(0);
		 player.setHealth(20);
		 player.setSaturation(20);
		 player.getInventory().clear();
		 player.sendTitle("�4�lLe gagnant est :�9�l " + joueur.getNom(), "�b avec " + joueur.getScore()+" kill(s). f�licitation", 40, 50, 20);
	}
	
}


}
