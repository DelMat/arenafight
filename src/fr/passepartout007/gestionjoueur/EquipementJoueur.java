package fr.passepartout007.gestionjoueur;

import java.io.Serializable;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class EquipementJoueur implements Serializable {
	

	String armure;
	String botte;
	String pantalon;
	String casque;
	String armePrincipale;
	String armeSecondaire;
	

	public EquipementJoueur() {
		this.CreerNewEquipement();
		
	}
	public void equipeJoueur(Player player) {
	
		ItemStack[] itemequipement;
		ItemStack armure= stringToItemStack(this.armure);
		ItemStack botte = stringToItemStack(this.botte);
		ItemStack pantalon = stringToItemStack(this.pantalon);
		ItemStack casque = stringToItemStack(this.casque);
	
		//ItemStack armePrincipale = stringToItemStack(this.armePrincipale);
		//ItemStack armeSecondaire = stringToItemStack(this.armeSecondaire);
		itemequipement = new ItemStack[] {botte,pantalon,armure,casque};
		
		player.getEquipment().setArmorContents(itemequipement);
		

	}
	public void CreerNewEquipement() {
		ItemStack[] itmequipement;
	
		botte = randowmEnchantingArmure(randowmBoots()).toString();
		armure = randowmEnchantingArmure(randowmChestplate()).toString();
		casque = randowmEnchantingArmure(randowmHelmet()).toString();
		pantalon = randowmEnchantingArmure(randowmLeggings()).toString();
		

	}
	
	private ItemStack randowmChestplate() {
		ItemStack itemStack = null;
		int Randomnumber = new Random().nextInt(5);
	
		//suivant le cas on fais des actione différente
		if (Randomnumber==0) {
				itemStack = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
			
			}
		if (Randomnumber==1){
				itemStack = new ItemStack(Material.DIAMOND_CHESTPLATE);
			}
		if (Randomnumber==2){
				itemStack = new ItemStack(Material.IRON_CHESTPLATE);
			}
		if (Randomnumber==3){
				itemStack = new ItemStack(Material.GOLDEN_CHESTPLATE);
			}
		if (Randomnumber==4){
				itemStack = new ItemStack(Material.LEATHER_CHESTPLATE);
			}
		

		return itemStack;
		
	}
	
	private ItemStack randowmBoots() {
		ItemStack itemStack = null;
		int Randomnumber = new Random().nextInt(5);
		//suivant le cas on fais des actione différente
		
		if (Randomnumber==0){
				itemStack = new ItemStack(Material.CHAINMAIL_BOOTS);
			}
		if (Randomnumber==1){
				itemStack =  new ItemStack(Material.DIAMOND_BOOTS);
			}
		if (Randomnumber==2){
				itemStack =  new ItemStack(Material.IRON_BOOTS);
			}
		if (Randomnumber==3){
				itemStack =  new ItemStack(Material.GOLDEN_BOOTS);
			}
		if (Randomnumber==4){
				itemStack = new ItemStack(Material.LEATHER_BOOTS);
			}
		
		return itemStack;
		
	}
	
	private ItemStack randowmHelmet() {
		ItemStack itemStack = null;
		int Randomnumber = new Random().nextInt(5);
		//suivant le cas on fais des actione différente

		if (Randomnumber==0){
				itemStack = new ItemStack(Material.CHAINMAIL_HELMET);
			}
		if (Randomnumber==1){
				itemStack = new ItemStack(Material.DIAMOND_HELMET);
			}
		if (Randomnumber==2){
				itemStack = new ItemStack(Material.IRON_HELMET);
			}
		if (Randomnumber==3){
				itemStack = new ItemStack(Material.GOLDEN_HELMET);
			}
		if (Randomnumber==4){
				itemStack = new ItemStack(Material.LEATHER_HELMET);
			}
		
		return itemStack;
		
	}
	
	private ItemStack randowmLeggings() {
		ItemStack itemStack = null;
		int Randomnumber = new Random().nextInt(5);
		//suivant le cas on fais des actione différente

		if (Randomnumber==0){
				itemStack = new ItemStack(Material.CHAINMAIL_LEGGINGS);
			}
		if (Randomnumber==1){
				itemStack = new ItemStack(Material.DIAMOND_LEGGINGS);
			}
		if (Randomnumber==2){
				itemStack = new ItemStack(Material.IRON_LEGGINGS);
			}
		if (Randomnumber==3){
				itemStack = new ItemStack(Material.GOLDEN_LEGGINGS);
			}
		if (Randomnumber==4){
				itemStack = new ItemStack(Material.LEATHER_LEGGINGS);
			}
		
		return itemStack;
		
	}

	private ItemStack randowmEnchantingArmure(ItemStack itemStack) {
		
		Material typearmur = itemStack.getData().getItemType();
		
		int radprotec = new Random().nextInt(3);
		if(radprotec != 0) {
			itemStack.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, radprotec );
		}
		int ramFaterfall = new Random().nextInt(3);
		

		if ( ramFaterfall != 0 && (typearmur == Material.LEGACY_LEATHER_BOOTS ||typearmur == Material.LEGACY_CHAINMAIL_BOOTS || typearmur == Material.LEGACY_DIAMOND_BOOTS ||typearmur == Material.LEGACY_IRON_BOOTS ||typearmur == Material.LEGACY_GOLD_BOOTS)) {						 
			itemStack.addEnchantment(Enchantment.PROTECTION_FALL, ramFaterfall );
		}
		
		itemStack.addEnchantment(Enchantment.DURABILITY, 3 );
		return itemStack;
	}

	private ItemStack stringToItemStack(String str) {
		String str1;

		if(str.contains(",")) {
			String[] STR = str.split(",");

			STR[0] = STR[0].substring(10);
			STR[0]	= STR[0].substring(0,STR[0].length()-4);
			str1= STR[0];
	
		}else {
			str1 = str.substring(10);
			str1	= str1.substring(0,str1.length()-5);
		}

		
		
		ItemStack itemstacks = new ItemStack(Material.valueOf(str1));
	
		return itemstacks;
		
	}
}
