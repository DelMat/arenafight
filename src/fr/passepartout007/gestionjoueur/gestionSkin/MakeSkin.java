package fr.passepartout007.gestionjoueur.gestionSkin;

import java.util.HashSet;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

import net.minecraft.server.v1_15_R1.EntityPlayer;
import net.minecraft.server.v1_15_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_15_R1.PacketPlayOutHeldItemSlot;
import net.minecraft.server.v1_15_R1.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_15_R1.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_15_R1.PacketPlayOutPosition;
import net.minecraft.server.v1_15_R1.PacketPlayOutRespawn;
import net.minecraft.server.v1_15_R1.PlayerConnection;
import net.minecraft.server.v1_15_R1.World;
import net.minecraft.server.v1_15_R1.WorldData;

public class MakeSkin {

	public void loadSkin(Player player,Skins skin) {
		Location l = player.getLocation();
	     CraftPlayer craftPlayer = (CraftPlayer) player;
		EntityPlayer entityPlayer= craftPlayer.getHandle();
		World world = entityPlayer.getWorld();
		GameProfile profile = entityPlayer.getProfile();
		 PlayerConnection playerConnection = entityPlayer.playerConnection;
		
		profile.getProperties().removeAll("textures");
		profile.getProperties().put("textures",new Property("textures",skin.getValue(),skin.getSignature()));
		
        PacketPlayOutPlayerInfo removePlayerPacket = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, entityPlayer);
        PacketPlayOutPlayerInfo addPlayerPacket = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, entityPlayer);
        PacketPlayOutEntityDestroy destroyEntityPacket = new PacketPlayOutEntityDestroy(player.getEntityId());
        PacketPlayOutNamedEntitySpawn entitySpawnPacket = new PacketPlayOutNamedEntitySpawn(entityPlayer);
        PacketPlayOutRespawn respawnEntityPacket = new PacketPlayOutRespawn (
                entityPlayer.dimension, WorldData.c(world.getWorldData().getSeed()), world.getWorldData().getType(), entityPlayer.playerInteractManager.getGameMode());
        PacketPlayOutPosition positionPacket = new PacketPlayOutPosition(l.getX(), l.getY(), l.getZ(), l.getYaw(), l.getPitch(), new HashSet<>(), 0);
        PacketPlayOutHeldItemSlot heldItemPacket = new PacketPlayOutHeldItemSlot(player.getInventory().getHeldItemSlot());
		for( Player onlinePlayer : Bukkit.getOnlinePlayers()) {
			
			if(!onlinePlayer.getUniqueId().equals(player.getUniqueId())) {
			
			       PlayerConnection connection = ((CraftPlayer) onlinePlayer).getHandle().playerConnection;
	                connection.sendPacket(removePlayerPacket);
	                connection.sendPacket(addPlayerPacket);
	                connection.sendPacket(destroyEntityPacket);
	                connection.sendPacket(entitySpawnPacket);
				
				
			}else {		           
		        playerConnection.sendPacket(removePlayerPacket);
		        playerConnection.sendPacket(addPlayerPacket);
		        playerConnection.sendPacket(respawnEntityPacket);
		        playerConnection.sendPacket(positionPacket);
		        playerConnection.sendPacket(heldItemPacket);
		        player.updateInventory();
		        craftPlayer.updateScaledHealth();
		        entityPlayer.triggerHealthUpdate();
			}
		}
		
	}
	
	
}
