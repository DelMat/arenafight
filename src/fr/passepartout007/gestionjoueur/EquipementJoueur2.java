package fr.passepartout007.gestionjoueur;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;

import fr.passepartout007.gestionbasedonnee.basededonne;
import fr.passepartout007.phasedujeux.ParametreDePartie;
import fr.passepartout007.scoreboard.GestionScoreboard;


public class EquipementJoueur2   {
	
	ArrayList<ItemStack> listItemStack = new ArrayList<ItemStack>();
	ItemStack armure;
	ItemStack botte;
	ItemStack pantalon;
	ItemStack casque;
	
	ItemStack armePrincipale;
	ItemStack armeSecondaire;
	ItemStack Heal;
	
	

	public EquipementJoueur2() {
		this.CreerNewEquipement();
		
	}
	public void equipeJoueur(Player player) {
		player.getInventory().clear();
		ItemStack[] itemequipement = new ItemStack[] {listItemStack.get(0),listItemStack.get(1),listItemStack.get(2),listItemStack.get(3)};
				player.getEquipment().setArmorContents(itemequipement);
				player.getInventory().addItem(listItemStack.get(4));
				player.getInventory().addItem(listItemStack.get(5));
				player.getInventory().addItem(new ItemStack(Material.ARROW,64));
				player.getInventory().addItem(new ItemStack(Material.BEEF,64));
				player.getInventory().addItem(listItemStack.get(6));
				player.getInventory().addItem(new ItemStack(Material.ENDER_PEARL,1));
				player.getInventory().addItem(new ItemStack(Material.WATER_BUCKET,1));
	}
	public void CreerNewEquipement() {
		
	
		botte = randowmEnchantingArmure(randowmBoots());
		armure = randowmEnchantingArmure(randowmChestplate());
		casque = randowmEnchantingArmure(randowmHelmet());
		pantalon = randowmEnchantingArmure(randowmLeggings());
		armePrincipale = randowmEnchantingArmePrincipal(randowmArmePrincipal());
		armeSecondaire = randowmEnchantingArmeSecondaire(randowmArmeSecondaire());
		Heal = randowmHeal();
	}
	private ItemStack randowmHeal() {
		ItemStack itemStack = null;
		int Randomnumber = new Random().nextInt(3);
		
		//suivant le cas on fais des actione différente
				if (Randomnumber!=0) {
						itemStack = new ItemStack(Material.GOLDEN_APPLE,Randomnumber);
					}else {
						itemStack = new ItemStack(Material.APPLE,1);
					}


		return itemStack;
	}
	private ItemStack randowmArmePrincipal() {
		ItemStack itemStack = null;
		int Randomnumber = new Random().nextInt(4);
		
		//suivant le cas on fais des actione différente
				if (Randomnumber==0) {
						itemStack = new ItemStack(Material.IRON_SWORD);
					
					}
				if (Randomnumber==1){
						itemStack = new ItemStack(Material.DIAMOND_SWORD);
					}
				if (Randomnumber==2){
					itemStack = new ItemStack(Material.DIAMOND_AXE);
				}
				if (Randomnumber==3){
					itemStack = new ItemStack(Material.IRON_AXE);
				}
		return itemStack;
	}
	
	private ItemStack randowmArmeSecondaire() {
		ItemStack itemStack = null;
		int Randomnumber = new Random().nextInt(3);
		
		//suivant le cas on fais des actione différente
				if (Randomnumber==0) {
						itemStack = new ItemStack(Material.BOW);
					}
				if (Randomnumber==1){
						itemStack = new ItemStack(Material.CROSSBOW);
					}
				if (Randomnumber==2){
					itemStack = new ItemStack(Material.TRIDENT);
				}

		return itemStack;
	}
	private ItemStack randowmChestplate() {
		ItemStack itemStack = null;
		int Randomnumber = new Random().nextInt(5);
		
		//suivant le cas on fais des actione différente
		if (Randomnumber==0) {
				itemStack = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
			
			}
		if (Randomnumber==1){
				itemStack = new ItemStack(Material.DIAMOND_CHESTPLATE);
			}
		if (Randomnumber==2){
				itemStack = new ItemStack(Material.IRON_CHESTPLATE);
			}
		if (Randomnumber==3){
				itemStack = new ItemStack(Material.GOLDEN_CHESTPLATE);
			}
		if (Randomnumber==4){
				itemStack = new ItemStack(Material.LEATHER_CHESTPLATE);
			}
		

		return itemStack;
		
	}
	
	private ItemStack randowmBoots() {
		ItemStack itemStack = null;
		int Randomnumber = new Random().nextInt(5);
		//suivant le cas on fais des actione différente
		
		if (Randomnumber==0){
				itemStack = new ItemStack(Material.CHAINMAIL_BOOTS);
			}
		if (Randomnumber==1){
				itemStack =  new ItemStack(Material.DIAMOND_BOOTS);
			}
		if (Randomnumber==2){
				itemStack =  new ItemStack(Material.IRON_BOOTS);
			}
		if (Randomnumber==3){
				itemStack =  new ItemStack(Material.GOLDEN_BOOTS);
			}
		if (Randomnumber==4){
				itemStack = new ItemStack(Material.LEATHER_BOOTS);
			}
		
		return itemStack;
		
	}
	
	private ItemStack randowmHelmet() {
		ItemStack itemStack = null;
		int Randomnumber = new Random().nextInt(5);
		//suivant le cas on fais des actione différente

		if (Randomnumber==0){
				itemStack = new ItemStack(Material.CHAINMAIL_HELMET);
			}
		if (Randomnumber==1){
				itemStack = new ItemStack(Material.DIAMOND_HELMET);
			}
		if (Randomnumber==2){
				itemStack = new ItemStack(Material.IRON_HELMET);
			}
		if (Randomnumber==3){
				itemStack = new ItemStack(Material.GOLDEN_HELMET);
			}
		if (Randomnumber==4){
				itemStack = new ItemStack(Material.LEATHER_HELMET);
			}
		
		return itemStack;
		
	}
	
	private ItemStack randowmLeggings() {
		ItemStack itemStack = null;
		int Randomnumber = new Random().nextInt(5);
		//suivant le cas on fais des actione différente

		if (Randomnumber==0){
				itemStack = new ItemStack(Material.CHAINMAIL_LEGGINGS);
			}
		if (Randomnumber==1){
				itemStack = new ItemStack(Material.DIAMOND_LEGGINGS);
			}
		if (Randomnumber==2){
				itemStack = new ItemStack(Material.IRON_LEGGINGS);
			}
		if (Randomnumber==3){
				itemStack = new ItemStack(Material.GOLDEN_LEGGINGS);
			}
		if (Randomnumber==4){
				itemStack = new ItemStack(Material.LEATHER_LEGGINGS);
			}
		
		return itemStack;
		
	}

	private ItemStack randowmEnchantingArmure(ItemStack itemStack) {
		
		Material typearmur = itemStack.getType();
		
		int radprotec = new Random().nextInt(3);
		if(radprotec != 0) {
			itemStack.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, radprotec );
		}
		int ramFaterfall = new Random().nextInt(3);
		
	
		if ( ramFaterfall != 0 && (typearmur == Material.LEGACY_LEATHER_BOOTS ||typearmur == Material.LEGACY_CHAINMAIL_BOOTS || typearmur == Material.LEGACY_DIAMOND_BOOTS ||typearmur == Material.LEGACY_IRON_BOOTS ||typearmur == Material.LEGACY_GOLD_BOOTS)) {						 
			itemStack.addEnchantment(Enchantment.PROTECTION_FALL, ramFaterfall );
		}
		
		itemStack.addEnchantment(Enchantment.DURABILITY, 3 );
		return itemStack;
	}
	private ItemStack randowmEnchantingArmePrincipal(ItemStack itemStack) {
		
		itemStack.addEnchantment(Enchantment.DURABILITY, 3 );
		if(itemStack.getType()==Material.IRON_SWORD|| itemStack.getType()==Material.DIAMOND_SWORD) {
			int RandowmSharp = new Random().nextInt(3);
			if(RandowmSharp != 0) {
				itemStack.addEnchantment(Enchantment.DAMAGE_ALL, RandowmSharp );
			}
			int Randowmfire = new Random().nextInt(2);
			if(Randowmfire != 0) {
				itemStack.addEnchantment(Enchantment.FIRE_ASPECT, Randowmfire );
			}
		}
		return itemStack;
		
		
	}
	private ItemStack randowmEnchantingArmeSecondaire(ItemStack itemStack) {
		
		itemStack.addEnchantment(Enchantment.DURABILITY, 3 );
		if(itemStack.getType()==Material.BOW) {
			itemStack.addEnchantment(Enchantment.ARROW_INFINITE,1);
			int RandowmPower = new Random().nextInt(3);
			if(RandowmPower != 0) {
				itemStack.addEnchantment(Enchantment.ARROW_DAMAGE, RandowmPower );
			}
			int Randowmfire = new Random().nextInt(2);
			if(Randowmfire != 0) {
				itemStack.addEnchantment(Enchantment.ARROW_FIRE, Randowmfire );
			}
		}
		if(itemStack.getType()==Material.CROSSBOW) {
			int RandowmMultishot = new Random().nextInt(2);
			if(RandowmMultishot != 0) {
				itemStack.addEnchantment(Enchantment.MULTISHOT, RandowmMultishot );
			}
			int Randowmccharge = new Random().nextInt(5);
			if(Randowmccharge != 0) {
				itemStack.addEnchantment(Enchantment.MULTISHOT, Randowmccharge );
			}
		}
		if(itemStack.getType()==Material.TRIDENT) {
			itemStack.addEnchantment(Enchantment.LOYALTY, 1 );
		}
		return itemStack;
		
		
	}
	public void exportEquipement() {
		
		listItemStack.clear();
		listItemStack.add(botte);
		listItemStack.add(pantalon);
		listItemStack.add(armure);
		listItemStack.add(casque);
		listItemStack.add(armePrincipale);
		listItemStack.add(armeSecondaire);
		listItemStack.add(Heal);
		basededonne bdd = new basededonne();
		String nomFichier = "EquipementPartie.txt";
		String chemin = bdd.CheminFichier(nomFichier);


		
		try {
			
		
		BukkitObjectOutputStream os = new BukkitObjectOutputStream(
										new BufferedOutputStream(
											new FileOutputStream(
												new File(chemin))));;
												
		os.writeObject(listItemStack);
		os.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public void ImportEquipement() {
		


		basededonne bdd = new basededonne();
		String nomFichier = "EquipementPartie.txt";
		String chemin = bdd.CheminFichier(nomFichier);


		
		try {
			
		
			BukkitObjectInputStream ois = new BukkitObjectInputStream(
		              						new BufferedInputStream(
		              							new FileInputStream(
		              								new File(chemin))));
												
			listItemStack =  (ArrayList<ItemStack>) ois.readObject();
		      ois.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
