package fr.passepartout007.gestionjoueur;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.passepartout007.gestionbasedonnee.ListdesJoueur;
import fr.passepartout007.gestionbasedonnee.basededonne;
import fr.passepartout007.gestionjoueur.gestionSkin.MakeSkin;
import fr.passepartout007.gestionjoueur.gestionSkin.Skins;
import fr.passepartout007.lancementplugin.lancement;
import fr.passepartout007.modeDeJeux.modeDejeux;
import fr.passepartout007.phasedujeux.ParametreDePartie;
import fr.passepartout007.phasedujeux.attenteduJeux;
import fr.passepartout007.scoreboard.GestionScoreboard;
import fr.passepartout007.scoreboard.ScoreboardAttenteDuJeux;

public class ConnectionDeconnectionJoueur implements Listener {
	transient lancement lcm;

	public ConnectionDeconnectionJoueur( lancement lancement) {
		this.lcm = lancement;
	}

	@EventHandler
	public void join(PlayerJoinEvent event){
		Player Player = event.getPlayer();
		Player.setCollidable(false);
		ListdesJoueur listJoueur = new ListdesJoueur();
		
		basededonne bdd = new basededonne();
	
		ParametreDePartie pdp= bdd.importParametrePartie();
		
		Joueur joueur = null;
		modeDejeux mdj = pdp.getModeDeJeux();
		if(pdp.isEncours()) {
			
			
			boolean traiter = false;
			for ( Joueur ldj : pdp.getListJoueur().getlistJoueur()) {
				if(ldj.getNom().equalsIgnoreCase(Player.getName())) {
					joueur = new Joueur(Player.getName(),ldj.getMode());
					listJoueur.addJoueurtolist(joueur);
					listJoueur.exportListOfJoueur();
					traiter = true;
				}
			}
			if(traiter == false) {
			
				joueur = new Joueur(Player.getName(),Mode.SPECTATEUR);
				listJoueur.addJoueurtolist(joueur);
				listJoueur.exportListOfJoueur();
			}
			if(joueur.getMode()== Mode.SPECTATEUR) {
				Player.setGameMode(GameMode.SPECTATOR);
				Player.teleport(pdp.getArene().aleaLocationSpawn());
				mdj.getScorboard().ScoreboardSpectateur(Player, "1", lcm);
			}else {
				Player.setGameMode(GameMode.SURVIVAL);
				Player.teleport(pdp.getArene().aleaLocationSpawn());

			}
			
		}else {
			 joueur = new Joueur(Player.getName(),Mode.JOUEUR);
				//ajouet a la liste des joueur
				listJoueur.addJoueurtolist(joueur);
				listJoueur.exportListOfJoueur();
				attenteduJeux attjeux = new attenteduJeux(lcm);
				Player.teleport(attjeux.getLoctionSpawn(Player.getWorld()));

		}
		
		System.out.println("le joueur " + Player.getName() +" rejoint le serveur");
		//ajou a la liste des joueur
		listJoueur.addJoueurtolist(joueur);
		listJoueur.exportListOfJoueur();
		pdp.setListJoueur(listJoueur);
		bdd.exportParametrePartie(pdp);
		//cr�ation des skin et chargement de skin
	
		ArrayList<Skins>  skins = Skins.listSkinConnection();;
		MakeSkin makeSkin = new MakeSkin();
		int Randomnumber = new Random().nextInt(skins.size());
		makeSkin.loadSkin(Player,skins.get(Randomnumber));
		
		// scoreboard
		ScoreboardAttenteDuJeux scb = new 	ScoreboardAttenteDuJeux();
		scb.ScoreboardJoueur(Player, "1", lcm);
		scb.afficher(Player);
		
		
		// gestion du scoreboard 
		GestionScoreboard gs = new GestionScoreboard();
		gs.MajScoreboardJeux(lcm);
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event){
		Player player =event.getPlayer();
		System.out.println("le joueur "+ player.getName() +" quitte");
		ListdesJoueur listJoueur = new ListdesJoueur();
		Joueur joueur = listJoueur.getJoueur(player);
		listJoueur.suppJoueurtolist(joueur);
		listJoueur.exportListOfJoueur();
		

		GestionScoreboard gs = new GestionScoreboard();
		gs.MajScoreboardJeux(lcm);
	}
}
