package fr.passepartout007.gestionjoueur;

import java.io.Serializable;

import org.bukkit.Location;

import fr.passepartout007.gestionterrain.Arena;

public class Joueur implements Serializable {

	private String nom;
	private Integer vie;
	private Integer score;
	private Mode mode;
	private Integer classement;
	private String equipe;
	private String locationspawn;
	
	
public Joueur(String nom,Mode mode) {
	this.setNom(nom);
	this.setMode(mode);
}


 public void setNom(String nom) {
	 this.nom = nom;
 }
 public String getNom() {
	 return this.nom;
 }
 
 public void setVie(Integer vie) {
	 this.vie = vie;
 }
 public Integer getVie() {
	 return this.vie;
 }
 public void setScore(Integer score) {
	 this.score = score;
 }
 public Integer getScore() {
	 return this.score;
 }
  
 public void setMode(Mode mode) {
	 this.mode = mode;
 }
 public Mode getMode() {
	 return this.mode;
 }
 
 public void setClassement(Integer classement) {
	 this.classement = classement;
 }
 public Integer getClassement() {
	 return this.classement;
  }


public Location getlocationspawn() {
	Arena arn = new Arena();
	
	return arn.stringToLocation(this.locationspawn);
}


public void setlocationspawn(Location location) {
	
	Arena arn = new Arena();

	this.locationspawn = arn.locattostring(location);
}
}
