package fr.passepartout007.gestionbasedonnee;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import fr.passepartout007.phasedujeux.ParametreDePartie;

public class basededonne {

	public String CheminFichier(String filename) {
	
		
		//recuperation du chemin et du nom
		String cheminPlugin = getClass().getProtectionDomain().getCodeSource().getLocation().getFile();

		String CheminFichier = cheminPlugin.replace(".","/");
		String[] tempo = CheminFichier.split("/");
		
		String nomPlugin = tempo[tempo.length -2];
		
		CheminFichier = CheminFichier.replace("jar",filename);

		CheminFichier = CheminFichier.replace("%20"," ");
		return CheminFichier;
   }
	
	public void exportParametrePartie(ParametreDePartie prmParti) {
		String nomFichier = "ParamPartie.txt";
		String chemin = this.CheminFichier(nomFichier);

		ObjectOutputStream oos;
		try {
			
			oos = new ObjectOutputStream(
		              new BufferedOutputStream(
		                      new FileOutputStream(
		                        new File(chemin))));
			
			oos.writeObject(prmParti);
			oos.close();
		
		} catch (FileNotFoundException e) {
		      e.printStackTrace();
	    } catch (IOException e) {
	      e.printStackTrace();
	    }   
		
	}
	
	public ParametreDePartie importParametrePartie() {

		String chemin = "ParamPartie.txt";
		chemin = this.CheminFichier(chemin);

		ObjectInputStream ois;
		ParametreDePartie parmPartie = null;
		
		try {
		      ois = new ObjectInputStream(
		              new BufferedInputStream(
		                new FileInputStream(
		                  new File(chemin))));
		      try {
		    	  
		    	  parmPartie  =  (ParametreDePartie) ois.readObject();	
		    	  
		      } catch (ClassNotFoundException e) {
		          e.printStackTrace();
		        }
		  	
		        ois.close();
		} catch (FileNotFoundException e) {
	    } catch (IOException e) {
	      e.printStackTrace();
	    } 
		

		return parmPartie;
	}
}
