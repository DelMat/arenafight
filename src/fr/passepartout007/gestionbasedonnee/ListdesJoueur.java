package fr.passepartout007.gestionbasedonnee;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import org.bukkit.entity.Player;

import fr.passepartout007.gestionjoueur.Joueur;

public class ListdesJoueur extends basededonne implements Serializable {
	
	String chemin = super.CheminFichier("ListJoueur.txt");
	public ListdesJoueur() {
		importListOfJoueur();
	}
	private ArrayList<Joueur> listJoueur = new ArrayList<Joueur>();
	
	
	public ArrayList<Joueur> getlistJoueursansImport(){
		return listJoueur;
	}
	public ArrayList<Joueur> getlistJoueur(){
		importListOfJoueur();
		return listJoueur;
	}
	public void addJoueurtolistSansImport(Joueur joueur) {

		if(!(joueurExist(joueur))){
			this.listJoueur.add(joueur);
			exportListOfJoueur();
		}
		
	}
	public void addJoueurtolist(Joueur joueur) {
		importListOfJoueur();
		if(!(joueurExist(joueur))){
			this.listJoueur.add(joueur);
			exportListOfJoueur();
		}
		
	}
	
	public void importListOfJoueur() {
		ObjectInputStream ois;
		
		try {
		      ois = new ObjectInputStream(
		              new BufferedInputStream(
		                new FileInputStream(
		                  new File(chemin))));
		      try {
		    	  this.listJoueur  = (ArrayList<Joueur>) ois.readObject();
		    	  
		      } catch (ClassNotFoundException e) {
		          e.printStackTrace();
		        }
		  	
		        ois.close();
		} catch (FileNotFoundException e) {
			exportListOfJoueur();
	    } catch (IOException e) {
	      e.printStackTrace();
	    } 
	}
	
	public void exportListOfJoueur() {
		ObjectOutputStream oos;
		try {
			
			oos = new ObjectOutputStream(
		              new BufferedOutputStream(
		                      new FileOutputStream(
		                        new File(chemin))));
			
			oos.writeObject(listJoueur);
			oos.close();
		
		} catch (FileNotFoundException e) {
		      e.printStackTrace();
	    } catch (IOException e) {
	      e.printStackTrace();
	    }   
	}

	public void suppJoueurtolist(Joueur joueur) {
		ArrayList<Joueur> listJoueurtemp = new ArrayList<Joueur>();
		importListOfJoueur();
		if(joueurExist(joueur)) {
			for (Joueur joueurs :listJoueur){
				if (!(joueurs.getNom().equalsIgnoreCase(joueur.getNom()))) {
					listJoueurtemp.add(joueurs);
				}
			}
			this.listJoueur = listJoueurtemp;
		}
		exportListOfJoueur();
	}
	
	public boolean joueurExist(Joueur joueur) {
		Boolean exist = false;
		for(Joueur joueurs : listJoueur) {
			if (joueurs.getNom().equals(joueur.getNom())) {
				exist = true;
			}			
		}
		return exist;
	}
	
	public Integer indexjoueur(Joueur joueur) {
	
		Integer i=0;
		for(Joueur joueurs : this.listJoueur) {
			if (joueurs.getNom().equals(joueur.getNom())) {
				i = this.listJoueur.indexOf(joueurs);
			}			
		}
		return i;
	}
	
	public boolean PlayerExist(Player player){

		importListOfJoueur();
		for(Joueur joueurs : this.listJoueur) {
			if(joueurs.getNom().equals(player.getName())) {
				return true;
			}
		}
		return false;
		
	}
	public Joueur getJoueur(Player player){
		Joueur rechjoueur = null;
		importListOfJoueur();
		for(Joueur joueurs : this.listJoueur) {
			if(joueurs.getNom().equals(player.getName())) {
				rechjoueur =joueurs;
			}
		}
		return rechjoueur;
		
	}
	
}
