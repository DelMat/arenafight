package fr.passepartout007.gestionbasedonnee;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import fr.passepartout007.gestionjoueur.Joueur;
import fr.passepartout007.phasedujeux.ParametreDePartie;
import fr.passepartout007.scoreboard.GestionScoreboard;

public class JumpSpeedBDD {
	public String CheminFichier(String filename) {
	
		
		//recuperation du chemin et du nom
		String cheminPlugin = getClass().getProtectionDomain().getCodeSource().getLocation().getFile();

		String CheminFichier = cheminPlugin.replace(".","/");
		String[] tempo = CheminFichier.split("/");
		
		String nomPlugin = tempo[tempo.length -2];
		
		CheminFichier = CheminFichier.replace("jar",filename);

		CheminFichier = CheminFichier.replace("%20"," ");
		return CheminFichier;
   }
	
	public void exportListButton(HashMap<Integer,String> map) {
		String nomFichier = "ListButton.txt";
		String chemin = this.CheminFichier(nomFichier);

		ObjectOutputStream oos;
		try {
			
			oos = new ObjectOutputStream(
		              new BufferedOutputStream(
		                      new FileOutputStream(
		                        new File(chemin))));
			
			oos.writeObject(map);
			oos.close();
		
		} catch (FileNotFoundException e) {
		      e.printStackTrace();
	    } catch (IOException e) {
	      e.printStackTrace();
	    }   
		
	}
	
	public HashMap<Integer,String> importListButton() {

		String chemin = "ListButton.txt";
		chemin = this.CheminFichier(chemin);

		ObjectInputStream ois;
		 HashMap<Integer,String>  map = null;
		
		try {
		      ois = new ObjectInputStream(
		              new BufferedInputStream(
		                new FileInputStream(
		                  new File(chemin))));
		      try {
		    	  
		    	  map  =  (HashMap<Integer,String>) ois.readObject();	
		    	  
		      } catch (ClassNotFoundException e) {
		          e.printStackTrace();
		        }
		  	
		        ois.close();
		} catch (FileNotFoundException e) {
	    } catch (IOException e) {
	      e.printStackTrace();
	    } 
		

		return map;
	}
	public Integer nbJoueurBouton(Integer score) {
		GestionScoreboard gs = new GestionScoreboard();
		basededonne bdd = new basededonne();
		ParametreDePartie paramPartie = bdd.importParametrePartie();
		ListdesJoueur listJoueur = paramPartie.getListJoueur();
		Integer nbJoueurwiththisnumButton =0;
		for(Joueur joueur :listJoueur.getlistJoueursansImport()) {
			if(joueur.getScore()==score) {
				nbJoueurwiththisnumButton++;
			}
		}
		return nbJoueurwiththisnumButton;
	}
}
