package fr.passepartout007.scoreboard;

import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.passepartout007.gestionbasedonnee.ListdesJoueur;
import fr.passepartout007.gestionbasedonnee.basededonne;
import fr.passepartout007.gestionjoueur.Joueur;
import fr.passepartout007.gestionjoueur.Mode;
import fr.passepartout007.lancementplugin.lancement;
import fr.passepartout007.modeDeJeux.modeDejeux;
import fr.passepartout007.phasedujeux.ParametreDePartie;

public class GestionScoreboard implements Listener {


	public GestionScoreboard() {
		
	}


	
	
	

	
	public void MajScoreboardJeux(lancement lancement){

		Collection<? extends Player> listPlayerOnline =  Bukkit.getOnlinePlayers();
		basededonne bdd = new basededonne();
		
		ParametreDePartie paramPartie = bdd.importParametrePartie();

		ListdesJoueur listJoueur = new ListdesJoueur();

		modeDejeux mdj = paramPartie.getModeDeJeux();

		for(Player player :listPlayerOnline) {

	
			if(listJoueur.PlayerExist(player)){
				Joueur joueur = listJoueur.getJoueur(player);
				
				if(joueur.getMode()==Mode.SPECTATEUR) {
			
					mdj.getScorboard().ScoreboardSpectateur(player, "0", lancement);
				} else if(joueur.getMode()==Mode.JOUEUR) {
	
					mdj.getScorboard().ScoreboardJoueur(player, "0", lancement);
				}
			}else{
				Joueur joueur = new Joueur(player.getName(), Mode.SPECTATEUR);
				mdj.getScorboard().ScoreboardSpectateur(player, player.getName() +"0", lancement);
			}
		}
	
	}
	
	
}
