package fr.passepartout007.scoreboard;

import java.io.Serializable;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;

import fr.passepartout007.gestionbasedonnee.ListdesJoueur;
import fr.passepartout007.gestionjoueur.Joueur;
import fr.passepartout007.lancementplugin.lancement;
import fr.passepartout007.phasedujeux.LancementDuJeux;
import net.minecraft.server.v1_15_R1.World;

public class  ScoreboardAttenteDuJeux extends ScoreBoards implements Serializable{
	

		public void ScoreboardJoueur(Player player, String objectiveName,lancement lancement) {
			super.ScoreboardJoueur(player, objectiveName, lancement);
	
			this.creer();
			player.setScoreboard(super.b);
	
		}

		public void ScoreboardSpectateur(Player player, String objectiveName,lancement lancement) {
			super.ScoreboardSpectateur(player, objectiveName, lancement);
			this.creer();
			player.setScoreboard(super.b);
		}

		public void creer() {
			super.detruire();
			super.creer();
			
			
			org.bukkit.World world = player.getWorld();
			Sign signNomArena = null;
			Sign signModeJeuxArena = null;
			LancementDuJeux ldj = new LancementDuJeux(player,super.lct);
			
			if(((org.bukkit.World) world).getBlockAt(ldj.locModeJeuxArena) != null && ((org.bukkit.World) world).getBlockAt(ldj.locNomArena) != null ){
				BlockState bslMJA = ((org.bukkit.World) world).getBlockAt(ldj.locModeJeuxArena).getState();
				BlockState bsNA = ((org.bukkit.World) world).getBlockAt(ldj.locNomArena).getState();
				
				if (bslMJA instanceof Sign && bsNA instanceof Sign){
					 signNomArena = (Sign) bslMJA;
					 signModeJeuxArena = (Sign) bsNA;
				}
			}
			String NomArene = signNomArena.getLine(2);
			String NomModeJeux = signModeJeuxArena.getLine(2);
			ListdesJoueur BddlistJoueur = new ListdesJoueur();
			BddlistJoueur.importListOfJoueur();
			Joueur joueur = null;
	
			if(BddlistJoueur.PlayerExist(player)) {
				 joueur = BddlistJoueur.getJoueur(player);
			}else {
				return;
			}
	
			Integer lignemax = 15;
			super.o.setDisplaySlot(DisplaySlot.SIDEBAR);
	
			super.o.getScore("�bTu est : �f" + joueur.getMode().toString()).setScore(lignemax-1);
			super.o.getScore("�4   ").setScore(lignemax-2);
			super.o.getScore("�e-----------------").setScore(lignemax-3);
			super.o.getScore("�4  ").setScore(lignemax-4);
			super.o.getScore("�bNb de joueur : �f" + super.nbJoueur()).setScore(lignemax-5);
			super.o.getScore("�bNb de spectateur : �f" + super.nbSpectateur()).setScore(lignemax-6);
			super.o.getScore("�4 ").setScore(lignemax-7);
			super.o.getScore("�e----------------").setScore(lignemax-8);
			super.o.getScore("�4").setScore(lignemax-9);
			super.o.getScore("�bL'ar�ne est : �f"+ NomArene).setScore(lignemax-10);
			super.o.getScore("�bLe mode de jeux est : " + NomModeJeux).setScore(lignemax-11);
			super.o.getScore(" ").setScore(lignemax-lignemax);
			ArrayList<String> Titre = new ArrayList<String>();
			Titre.add(0,"�4�lA�6�lr�e�le�e�lnaFigh�6�lt");
			Titre.add(1, "�6�lA�4�lr�6�len�e�laFight");
			Titre.add(2, "�e�lA�6�lr�4�le�6�ln�e�laFight");
			Titre.add(3, "�e�lAr�6�le�4�ln�6�la�e�lFight");
			Titre.add(4, "�e�lAre�6�ln�4�la�6�lF�e�light");
			Titre.add(5, "�e�lAren�6�la�4�lF�6�li�e�lght");
			Titre.add(6, "�e�lArena�6�lF�4�li�6�lg�e�lht");
			Titre.add(7, "�e�lArenaF�6�li�4�lg�6�lh�e�lt");
			Titre.add(8, "�e�lArenaFi�6�lg�4�lh�6�lt");
			Titre.add(9, "�6�lA�e�lrenaFig�6�lh�4�lt");
			super.o.setDisplayName(Titre.get(0));
			Objective o = super.o;
			new BukkitRunnable() {
				int compteur = 0;
				
				@Override
				public void run() {	
					if (compteur < Titre.size()-1) {
						compteur++;
						o.setDisplayName(Titre.get(compteur));
					
					}else {
						o.setDisplayName(Titre.get(0));
						 compteur = 0;
					}
					
				}
			}.runTaskTimer(super.lct,0, 2);
			
			super.o = o;
			
		
		}
		
}
