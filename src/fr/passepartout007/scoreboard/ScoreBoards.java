package fr.passepartout007.scoreboard;

import java.io.Serializable;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import fr.passepartout007.gestionbasedonnee.ListdesJoueur;
import fr.passepartout007.gestionjoueur.Joueur;
import fr.passepartout007.gestionjoueur.Mode;
import fr.passepartout007.lancementplugin.lancement;



public class ScoreBoards implements Serializable {
	public Boolean Creer = false;
	public ScoreboardManager m;
	public Scoreboard b;
	public Objective o;
	public Player player;
	public String objectiveName;
	public transient lancement lct;

	


	public void ScoreboardJoueur(Player player, String objectiveName,lancement lancement) {
		this.player = player;
		this.objectiveName= objectiveName;
		this.lct= lancement;
	}

	public void ScoreboardSpectateur(Player player, String objectiveName,lancement lancement) {
		this.player = player;
		this.objectiveName= objectiveName;
		this.lct= lancement;
	}
	
	//creer un scorebord
	
	public void creer() {
		if (Creer)return;
		this.detruire();
		this.m = Bukkit.getScoreboardManager();
		this.b = m.getNewScoreboard();
		this.o = b.registerNewObjective(this.objectiveName, this.objectiveName, this.objectiveName);
		for (int i = 1 ; i <= 15 ;i++) {
			o.getScore("").setScore(i);
		}
		Creer=true;
	}
	
	public void detruire() {
		if (!this.Creer)return;
		player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
		this.Creer = false;
	}
	public void afficher(Player player) {
		player.setScoreboard(this.b);
	}
	
	public Integer nbJoueur() {
		ListdesJoueur listDesJoueurs =new ListdesJoueur();
		
		Integer nbJoueur =0;
		for(Joueur joueurs:listDesJoueurs.getlistJoueur()) {
			if(joueurs.getMode()==Mode.JOUEUR) {
			
				nbJoueur++;
			}
		}
		return nbJoueur;
	}
	public Integer nbSpectateur() {
		ListdesJoueur listDesJoueurs =new ListdesJoueur();
		
		Integer nbSpect =0;
		for(Joueur joueurs:listDesJoueurs.getlistJoueur()) {
			if(joueurs.getMode()==Mode.SPECTATEUR) {

				nbSpect++;
			}
		}
		return nbSpect;
	}


}
