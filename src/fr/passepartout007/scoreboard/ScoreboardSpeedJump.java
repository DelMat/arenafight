package fr.passepartout007.scoreboard;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;

import fr.passepartout007.gestionbasedonnee.JumpSpeedBDD;
import fr.passepartout007.gestionbasedonnee.ListdesJoueur;
import fr.passepartout007.gestionbasedonnee.basededonne;
import fr.passepartout007.gestionjoueur.Joueur;
import fr.passepartout007.gestionjoueur.Mode;
import fr.passepartout007.lancementplugin.lancement;
import fr.passepartout007.phasedujeux.ParametreDePartie;

public class ScoreboardSpeedJump extends ScoreBoards implements Serializable{
	
	
	public void ScoreboardJoueur(Player player, String objectiveName,lancement lancement) {
		super.ScoreboardJoueur(player, objectiveName, lancement);
		this.creerJoueur();
		player.setScoreboard(super.b);

	}

	public void ScoreboardSpectateur(Player player, String objectiveName,lancement lancement) {
		super.ScoreboardSpectateur(player, objectiveName, lancement);
		this.creerSpectateur();
		player.setScoreboard(super.b);
	
	}

	public void creerJoueur() {

		super.detruire();
		super.creer();
	
		basededonne bdd = new basededonne();
		ParametreDePartie PDP = bdd.importParametrePartie();


		ListdesJoueur BddlistJoueur = PDP.getListJoueur();
		
		Joueur joueur = null;
		Collection<? extends Player> listPlayerOnline =  Bukkit.getOnlinePlayers();

		if(BddlistJoueur.PlayerExist(player)) {
			 joueur = BddlistJoueur.getJoueur(player);
		}else{
			return;
		}

		Integer lignemax = 15;
		super.o.setDisplaySlot(DisplaySlot.SIDEBAR);

		super.o.getScore("�bLe mode est : "  + PDP.getModeDeJeux().getName()).setScore(lignemax-1);
		super.o.getScore("�4   ").setScore(lignemax-2);
		super.o.getScore("�e-----------------").setScore(lignemax-3);

		JumpSpeedBDD JsBDD = new JumpSpeedBDD();
		 HashMap<Integer,String> listbutton =  JsBDD.importListButton();
		 int ligne = 4;
		 for( Entry<Integer, String> data: listbutton.entrySet()) {
			 if (ligne+3==lignemax-2) {
				 break;
			 }
			 int nbJoueurCheckpoint = 0;
	
			 for( Joueur joueurs : BddlistJoueur.getlistJoueur()) {
				 if(joueurs.getMode()==Mode.JOUEUR) {
					 if (joueurs.getScore().equals(data.getKey())) {
	
						 nbJoueurCheckpoint++;
					 }
				 }
			 }

			 super.o.getScore("�bCheckPoints n�"  +data.getKey() + " �f: " + nbJoueurCheckpoint + " personne").setScore(ligne);
			 
		ligne++;	 
		 }
		 super.o.getScore("�e----------------- ").setScore(1);
		 super.o.getScore("�bTu es au checkpoint n� �f" +joueur.getScore()).setScore(0);
		ArrayList<String> Titre = new ArrayList<String>();
		Titre.add(0,"�4�lA�6�lr�e�le�e�lnaFigh�6�lt");
		Titre.add(1, "�6�lA�4�lr�6�len�e�laFight");
		Titre.add(2, "�e�lA�6�lr�4�le�6�ln�e�laFight");
		Titre.add(3, "�e�lAr�6�le�4�ln�6�la�e�lFight");
		Titre.add(4, "�e�lAre�6�ln�4�la�6�lF�e�light");
		Titre.add(5, "�e�lAren�6�la�4�lF�6�li�e�lght");
		Titre.add(6, "�e�lArena�6�lF�4�li�6�lg�e�lht");
		Titre.add(7, "�e�lArenaF�6�li�4�lg�6�lh�e�lt");
		Titre.add(8, "�e�lArenaFi�6�lg�4�lh�6�lt");
		Titre.add(9, "�6�lA�e�lrenaFig�6�lh�4�lt");
		super.o.setDisplayName(Titre.get(0));
		Objective o = super.o;
		new BukkitRunnable() {
			int compteur = 0;
			
			@Override
			public void run() {	
				if (compteur < Titre.size()-1) {
					compteur++;
					o.setDisplayName(Titre.get(compteur));
				
				}else {
					o.setDisplayName(Titre.get(0));
					 compteur = 0;
				}
				
			}
		}.runTaskTimer(super.lct,0, 2);
		
		super.o = o;
		
	
	}
	public void creerSpectateur() {

		super.detruire();
		super.creer();
	
		basededonne bdd = new basededonne();
		ParametreDePartie PDP = bdd.importParametrePartie();


		ListdesJoueur BddlistJoueur = PDP.getListJoueur();
		
		Joueur joueur = null;
		Collection<? extends Player> listPlayerOnline =  Bukkit.getOnlinePlayers();

		if(BddlistJoueur.PlayerExist(player)) {
			 joueur = BddlistJoueur.getJoueur(player);
		}else{
			return;
		}

		Integer lignemax = 15;
		super.o.setDisplaySlot(DisplaySlot.SIDEBAR);

		super.o.getScore("�bLe mode est : "  + PDP.getModeDeJeux().getName()).setScore(lignemax-1);
		super.o.getScore("�4   ").setScore(lignemax-2);
		super.o.getScore("�e-----------------").setScore(lignemax-3);

		JumpSpeedBDD JsBDD = new JumpSpeedBDD();
		 HashMap<Integer,String> listbutton =  JsBDD.importListButton();
		 int ligne = 4;
		 for( Entry<Integer, String> data: listbutton.entrySet()) {
			 if (ligne+3==lignemax-2) {
				 break;
			 }
			 int nbJoueurCheckpoint = 0;
			 for( Joueur joueurs : BddlistJoueur.getlistJoueur()) {
				 if (joueurs.getScore().equals(data.getKey())) {

					 nbJoueurCheckpoint++;
				 }
			 }

			 super.o.getScore("�bCheckPoints n�"  +data.getKey() + " �f: " + nbJoueurCheckpoint + " personne").setScore(ligne);
			 
		ligne++;	 
		 }

		ArrayList<String> Titre = new ArrayList<String>();
		Titre.add(0,"�4�lA�6�lr�e�le�e�lnaFigh�6�lt");
		Titre.add(1, "�6�lA�4�lr�6�len�e�laFight");
		Titre.add(2, "�e�lA�6�lr�4�le�6�ln�e�laFight");
		Titre.add(3, "�e�lAr�6�le�4�ln�6�la�e�lFight");
		Titre.add(4, "�e�lAre�6�ln�4�la�6�lF�e�light");
		Titre.add(5, "�e�lAren�6�la�4�lF�6�li�e�lght");
		Titre.add(6, "�e�lArena�6�lF�4�li�6�lg�e�lht");
		Titre.add(7, "�e�lArenaF�6�li�4�lg�6�lh�e�lt");
		Titre.add(8, "�e�lArenaFi�6�lg�4�lh�6�lt");
		Titre.add(9, "�6�lA�e�lrenaFig�6�lh�4�lt");
		super.o.setDisplayName(Titre.get(0));
		Objective o = super.o;
		new BukkitRunnable() {
			int compteur = 0;
			
			@Override
			public void run() {	
				if (compteur < Titre.size()-1) {
					compteur++;
					o.setDisplayName(Titre.get(compteur));
				
				}else {
					o.setDisplayName(Titre.get(0));
					 compteur = 0;
				}
				
			}
		}.runTaskTimer(super.lct,0, 2);
		
		super.o = o;
	}	
}
